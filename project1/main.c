#include "p1_hy486.h"

/*----------------------------------------------------------------*/
/*DEBUG FUNCTIONS START */
/*prints how the program is used in case of incorrect input */
void usage(void)
{
  printf(
      "\n"
      "Usage:\n"
      "    songmanager -t thread_count\n"
      "e.g songmanager -t 4"

  );
  exit(EXIT_FAILURE);
}
/*changes the text color to red */
void red()
{
  printf("\033[1;31m");
}
/*changes the text color to green */
void green()
{
  printf("\033[0;32m");
}
/*resets the text color */
void reset()
{
  printf("\033[0m");
}

/*used for testing if threads are made correctly*/
void printthreadList(struct song_manager *node)
{
  while (node != NULL)
  {
    printf(" %d, ", node->song_manager_id);
    node = node->next;
  }
  printf("\n");
}

/*used for testing contents of shared list*/
void printsharedList()
{
  struct listNode *curr;
  curr = malloc(sizeof(struct listNode));
  curr = sharedlist->head->next;
  printf("Shared_List:");
  while (curr != sharedlist->tail)
  {
    printf(" %d, ", curr->songID);
    curr = curr->next;
  }

  printf("\n");
}

/*prints the contents of the entire tree, usefull for debugging*/
void print_tree(struct node *root)
{
  if (root != NULL)
  {
    if (is_2_node(root))
    {
      print_tree(root->child1);
      printf("%d \n ", root->key1);
      print_tree(root->child2);
    }
    else if (is_3_node(root))
    {
      print_tree(root->child1);
      printf("%d  \n ", root->key1);
      print_tree(root->child2);
      printf("%d  \n ", root->key2);
      print_tree(root->child3);
    }
    else
    {
      print_tree(root->child1);
      printf("%d  \n ", root->key1);
      print_tree(root->child2);
      printf("%d  \n ", root->key2);
      print_tree(root->child3);
      printf("%d  \n ", root->key3);
      print_tree(root->child4);
    }
  }
}

/*prints certain levels of the tree in 2-3-4 format used for debugging*/
void Debug_print_tree(struct node *node, int depth)
{
  printf("\n");
  printf("depth %d:%d,%d,%d \n", depth, node->key1, node->key2, node->key3);
  if (node->child1 != NULL)
  {
    Debug_print_tree(node->child1, depth + 1);
  }
  if (node->child2 != NULL)
  {
    Debug_print_tree(node->child2, depth + 1);
  }
  if (node->child3 != NULL)
  {
    Debug_print_tree(node->child3, depth + 1);
  }
  if (node->child4 != NULL)
  {
    Debug_print_tree(node->child4, depth + 1);
  }
}

/*prints every element of a queue*/
void print_queue_debug(struct queue *Q)
{
  struct queueNode *current = malloc(sizeof(struct queueNode));
  current = Q->Head->next; /*since head is sentinel node containing garbage*/
  printf("\nThese are the song_ids I have in the Queue:");
  while (current != NULL)
  {
    printf("%d,", current->songID);
    current = current->next;
  }
  printf("\n");
}

/*prints all queues*/
void print_queues(struct concurrent_queue_list *Q)
{
  struct concurrent_queue_list *current = malloc(sizeof(struct concurrent_queue_list));
  current = Q;
  int i = 0;
  for (i = 0; i < thread_count / 2; i++)
  {
    print_queue_debug(current->queue);
    current = current->next;
  }
}

/*DEBUG FUNCTIONS END */
/*----------------------------------------------------------------*/

/*----------------------------------------------------------------*/
/*LIST FUNCTIONS START*/

/*shared list validate*/
int validate(struct listNode *pred, struct listNode *curr)
{
  if (pred->marked == 0 && curr->marked == 0 && pred->next == curr)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

/*creates a new list node*/
struct listNode *new_list_node(int key)
{
  struct listNode *new;
  new = malloc(sizeof(struct listNode));
  new->marked = 0;
  new->next = NULL;
  new->songID = key;
  pthread_mutex_init(&(new->lock), NULL);
  return new;
}

/*insert key into shared lazy list*/
int list_insert(int key)
{ // code for process p
  struct listNode *curr;
  curr = malloc(sizeof(struct listNode));
  struct listNode *pred;
  pred = malloc(sizeof(struct listNode));

  int result;
  int return_flag = 0;
  while (1)
  {
    pred = sharedlist->head;
    curr = pred->next;
    while (curr->next != NULL && curr->songID < key)
    {
      pred = curr;
      curr = curr->next;
    }
    pthread_mutex_lock(&(pred->lock));
    if (curr != NULL)
    {
      pthread_mutex_lock(&(curr->lock));
    }
    if (validate(pred, curr) == 1)
    {
      if (key == curr->songID)
      {
        result = 0;
        return_flag = 1;
      }
      else
      {
        struct listNode *node;
        node = new_list_node(0);
        node->next = curr;
        node->songID = key;
        pred->next = node;
        result = 1;
        return_flag = 1;
      }
    }
    pthread_mutex_unlock(&(pred->lock));
    if (curr != NULL)
    {
      pthread_mutex_unlock(&(curr->lock));
    }
    if (return_flag)
    {
      return result;
    }
  }
}

/*LIST FUNCTIONS END*/
/*----------------------------------------------------------------*/

/*----------------------------------------------------------------*/
/*Queue FUNCTIONS START*/

/*make a new Queue*/
struct queue *make_queue()
{
  struct queue *Q;
  Q = malloc(sizeof(struct queue));
  init(Q);
  return Q;
}

/*initialiaze elements of Queue*/
void init(struct queue *Q)
{

  struct queueNode *p = malloc(sizeof(struct queueNode));

  // sentinel node
  p->next = NULL;
  Q->Head = p;
  Q->Tail = p;
}

/*Create a new list containing all queues*/
void make_queue_list(int M)
{
  head_queue = malloc(sizeof(struct concurrent_queue_list));
  head_queue->next = NULL;
  head_queue->queue = make_queue();
  struct concurrent_queue_list *current = head_queue;

  while (M > 1)
  {
    while (current->next != NULL)
    {
      current = current->next;
    }
    current->next = malloc(sizeof(struct concurrent_queue_list));
    current->next->next = NULL;
    current->next->queue = make_queue();
    M--;
  }
  while (current->next != NULL)
  {
    current = current->next;
  }
  current->next = head_queue;
}

/*enq\insert value to queue Q*/
void enq(struct queue *Q, int value)
{

  struct queueNode *next = malloc(sizeof(struct queueNode));
  struct queueNode *last = malloc(sizeof(struct queueNode));
  struct queueNode *p = malloc(sizeof(struct queueNode));

  p->songID = value;
  p->next = NULL;
  while (1)
  {                    // keep trying until enq() is done
    last = Q->Tail;    // read Tail
    next = last->next; // read next node of last
    if (last == Q->Tail)
    { // are last and next consistent?
      if (next == NULL)
      { // was Tail pointing to the last node?
        if (__sync_bool_compare_and_swap(&last->next, next, p))
        { // try to link new node at the end of the list
          break;
        }
      }
      else
      {
        __sync_bool_compare_and_swap(&Q->Tail, last, next);
      }                                            // tail was not pointing to last node; try to advance
    }                                              // if
  }                                                // while
  __sync_bool_compare_and_swap(&Q->Tail, last, p); // equeue is done -> try to swing Tail to the inserted node
} // Enqueue

/*deq/delete value from queue Q*/
int deq(struct queue *Q, int *pvalue)
{
  struct queueNode *first = malloc(sizeof(struct queueNode));
  struct queueNode *last = malloc(sizeof(struct queueNode));
  struct queueNode *next = malloc(sizeof(struct queueNode));
  int i;
  while (1)
  {                  // keep trying until deq() is done
    first = Q->Head; // read Head
    last = Q->Tail;  // read Tail
    next = first->next;
    if (first == Q->Head)
    { // are first and next still consistent?
      if (first == last)
      { // is queue empty or Tail falling behind?
        if (next == NULL)
        { // is queue empty?
          return 0;
        }
        __sync_bool_compare_and_swap(&Q->Tail, last, next); // tail is falling behind -> try to advance it
      }
      else
      { // no need to deal with Tail
        //printf("\nWhat is next->songId? %d",next->songID);
        list_insert(next->songID);
        for (i = 0; i < 6; i++)
        {
          BSTDelete(next->songID);
        }
        *pvalue = next->songID; // read value to return
        if (__sync_bool_compare_and_swap(&Q->Head, first, next))
        { // try to swing Head to the next node
          break;
        }
      }     // else // deq() is done, so exit loop
    }       // if
  }         // while
  return 1; // queue was not empty, so deq() suceeded
} // Dequeue

/*QUEUE FUNCTIONS END*/
/*----------------------------------------------------------------*/

/*----------------------------------------------------------------*/
/*THREAD MANAGERS FUNCTIONS START*/

/*initialized a new song manager thread*/
void initialize_song_manager(struct song_manager *c, int id)
{
  c->song_manager_id = id;
  c->next = NULL;
}

/*creates the list containing the song managers\users threads*/
void make_song_managers_list(struct song_manager *head, int count)
{
  struct song_manager *current = head;
  int id = 1;
  while (count > 1)
  {
    while (current->next != NULL)
    {
      current = current->next;
    }

    /* now we can add a new variable */
    current->next = malloc(sizeof(struct song_manager));
    initialize_song_manager(current->next, id);
    id++;
    count--;
  }
}

/*each thread begins inserting into 2-3-4 tree*/
void start_inserting_tree(struct song_manager *head)
{
  struct song_manager *current = head;
  while (current != NULL)
  {
    pthread_create(&current->thread, &attr, Song_generator, current);

    current = current->next;
  }
}
/*each thread begins inserting into lock free queues*/
void start_inserting_queues(struct song_manager *head)
{
  int M = thread_count / 2;
  //printf("\nI am going to create %d queues\n", M);
  make_queue_list(M);

  struct song_manager *current = head;
  while (current != NULL)
  {
    pthread_create(&current->thread, &attr, Song_user_queue_insert, current);

    current = current->next;
  }
}

/*each thread begins the delete ops into 2-3-4 tree*/
void start_deleting(struct song_manager *head)
{

  struct song_manager *current = head;
  while (current != NULL)
  {
    pthread_create(&current->thread, &attr, Song_manager_delete, current);

    current = current->next;
  }
}

/*inserts songs into 2-3-4 tree based on songmanager_id and thread count*/
void *Song_generator(void *curr_thread)
{
  int i = 0;
  struct song_manager *mydata;
  mydata = (struct song_manager *)curr_thread;

  for (i = 0; i < thread_count; i++)
  {
    //for(i=0;i<20;i++){

    BSTInsert(i * thread_count + (mydata->song_manager_id));
    //Debug_print_tree();
  }
  pthread_barrier_wait(&first_barrier);
}

/*inserts songs in queue in the way requested by the exercise*/
void *Song_user_queue_insert(void *curr_thread)
{
  struct song_manager *mydata;
  mydata = (struct song_manager *)curr_thread;
  int i = 0;
  int j = 0;
  struct concurrent_queue_list *current = head_queue;
  for (j = 0; j < ((mydata->song_manager_id + 1) % (thread_count / 2)); j++)
  {
    current = current->next;
  }

  for (i = 0; i < thread_count; i++)
  {
    if (search((thread_count * mydata->song_manager_id) + i, root))
    {
      enq(current->queue, (thread_count * mydata->song_manager_id) + i);
      current = current->next;
    }
  }

  pthread_barrier_wait(&second_barrier);
}

/*deqs from queues, deletes from tree and inserts in list*/
/*Because of the nature of the 2-3-4 tree delete from tree
and insert in shared list are done inside deq*/
void *Song_manager_delete(void *curr_thread)
{
  struct song_manager *mydata;
  mydata = (struct song_manager *)curr_thread;
  int i = 0;
  int j = 0;
  struct concurrent_queue_list *current = head_queue;
  for (j = 0; j < ((mydata->song_manager_id + 1) % (thread_count / 2)); j++)
  {
    current = current->next;
  }
  for (i = 0; i < thread_count / 2; i++)
  {

    while (!deq(current->queue, &current->queue->Head->next->songID))
    {
    }

    //list_insert((thread_count * mydata->song_manager_id) + i);
    current = current->next;
  }

  pthread_barrier_wait(&fourth_barrier);
}
/*QUEUE FUNCTIONS END*/
/*----------------------------------------------------------------*/

/*----------------------------------------------------------------*/
/*ERROR SIZE AND KEYSUM CHECKING FUNCTIONS START*/

/*checks tree size and keysum after completion of insert*/
void *check_tree_size_keysum()
{
  Tree_Size_Check(root);
  if (thread_count * thread_count == total_songs)
  {
    green();
    printf("\nTotal size check passed (expected:%d , found: %d)\n", thread_count * thread_count, total_songs);
    reset();
  }
  else
  {
    red();
    printf("Total size check ERROR (expected:%d , found: %d)\n", thread_count * thread_count, total_songs);
    reset();
  }
  
  keysum(root);
  if (thread_count * thread_count * (thread_count - 1) * (thread_count + 1) / 2 == keysum_n)
  {
    green();
    printf("\nTotal keysum check passed (expected:%d , found: %d) \n", thread_count * thread_count * (thread_count - 1) * (thread_count + 1) / 2, keysum_n);
    reset();
  }
  else
  {
    red();
    printf("\nTotal keysum check ERROR (expected:%d , found: %d) \n", thread_count * thread_count * (thread_count - 1) * (thread_count + 1) / 2, keysum_n);
    reset();
  }
}

/*checks sizes after completion of delete*/
void *check_after_delete()
{
  total_songs = 0;
  Tree_Size_Check(root);
  if ((thread_count * thread_count / 2) == total_songs)
  {
    green();
    printf("\nTree size check passed (expected:%d , found: %d)\n", thread_count * thread_count / 2, total_songs);
    reset();
  }
  else
  {
    red();
    printf("Tree size check ERROR (expected:%d , found: %d)\n", thread_count * thread_count / 2, total_songs);
    reset();
  }
  int size = sharedList_size();

  if (((thread_count * thread_count) / 2) == size)
  {
    green();
    printf("\nlist’s size check passed (expected:%d , found: %d)\n", ((thread_count * thread_count) / 2), size);
    reset();
  }
  else
  {
    red();
    printf("list’s size check ERROR (expected:%d , found: %d)\n", ((thread_count * thread_count) / 2), size);
    reset();
  }
  after_delete = 1;
  total_queues_size = 0;
  struct concurrent_queue_list *current = malloc(sizeof(struct concurrent_queue_list));
  current = head_queue;
  int i = 0;
  for (i = 0; i < thread_count / 2; i++)
  {
    total_queue_size(current->queue);
    current = current->next;
  }

  if (((thread_count * thread_count) / 2) == total_queues_size)
  {
    green();
    printf("\nqueues’ total size check passed (expected:%d , found: %d)\n", ((thread_count * thread_count) / 2), total_queues_size);
    reset();
  }
  else
  {
    red();
    printf("queues’ total size check ERROR (expected:%d , found: %d)\n", ((thread_count * thread_count) / 2), total_queues_size);
    reset();
  }

}

/*the function called to calculate ammount of songs in tree*/
void Tree_Size_Check(struct node *root)
{

  if (root != NULL)
  {
    if (is_2_node(root))
    {
      Tree_Size_Check(root->child1);
      total_songs++;
      Tree_Size_Check(root->child2);
    }
    else if (is_3_node(root))
    {
      Tree_Size_Check(root->child1);
      total_songs++;
      Tree_Size_Check(root->child2);
      total_songs++;
      Tree_Size_Check(root->child3);
    }
    else
    {
      Tree_Size_Check(root->child1);
      total_songs++;
      Tree_Size_Check(root->child2);
      total_songs++;
      Tree_Size_Check(root->child3);
      total_songs++;
      Tree_Size_Check(root->child4);
    }
  }
}

/*checks shared list size after completion of list insert*/
void *check_list_size()
{
  int size = sharedList_size();

  if (((thread_count * thread_count) / 2) == size)
  {
    green();
    printf("\nlist’s size check passed (expected:%d , found: %d)\n", ((thread_count * thread_count) / 2), size);
    reset();
  }
  else
  {
    red();
    printf("list’s size check ERROR (expected:%d , found: %d)\n", ((thread_count * thread_count) / 2), size);
    reset();
  }
}

/*function used to calculate num of elements inside list*/
int sharedList_size()
{
  int count = 0;
  struct listNode *curr;
  curr = malloc(sizeof(struct listNode));
  curr = sharedlist->head->next;

  while (curr != sharedlist->tail)
  {
    count++;
    curr = curr->next;
  }

  return count;
}

/*checks queues size after completion of insert*/
void *check_queues_size()
{

  struct concurrent_queue_list *current = malloc(sizeof(struct concurrent_queue_list));
  current = head_queue;
  int i = 0;
  for (i = 0; i < thread_count / 2; i++)
  {
    total_queue_size(current->queue);
    current = current->next;
  }

  if (thread_count * thread_count == total_queues_size)
  {
    green();
    printf("\nqueues’ total size check passed (expected:%d , found: %d)\n", thread_count * thread_count, total_queues_size);
    reset();
  }
  else
  {
    red();
    printf("queues’ total size check ERROR (expected:%d , found: %d)\n", thread_count * thread_count, total_queues_size);
    reset();
  }
  
  current = head_queue;
  
  for (i = 0; i < thread_count / 2; i++)
  {
    total_queue_keysum(current->queue);
    current = current->next;
  }

  if (thread_count * thread_count * (thread_count - 1) * (thread_count + 1) / 2 == keysum_queues)
  {
    green();
    printf("\n queues Total keysum check passed (expected:%d , found: %d) \n", thread_count * thread_count * (thread_count - 1) * (thread_count + 1) / 2, keysum_queues);
    reset();
  }
  else
  {
    red();
    printf("\n queues Total keysum check ERROR (expected:%d , found: %d) \n", thread_count * thread_count * (thread_count - 1) * (thread_count + 1) / 2, keysum_queues);
    reset();
  }
  pthread_barrier_wait(&third_barrier);
}

/*checks queues size after completion of delete\deq*/
void *check_queues_size_after_delete()
{
  after_delete = 1;
  total_queues_size = 0;
  struct concurrent_queue_list *current = malloc(sizeof(struct concurrent_queue_list));
  current = head_queue;
  int i = 0;
  for (i = 0; i < thread_count / 2; i++)
  {
    total_queue_size(current->queue);
    current = current->next;
  }

  if (((thread_count * thread_count) / 2) == total_queues_size)
  {
    green();
    printf("\nqueues’ total size check passed (expected:%d , found: %d)\n", ((thread_count * thread_count) / 2), total_queues_size);
    reset();
  }
  else
  {
    red();
    printf("queues’ total size check ERROR (expected:%d , found: %d)\n", ((thread_count * thread_count) / 2), total_queues_size);
    reset();
  }
}

/*calculates num of songs in each queue and displays error message
if queue of unexpected size is found*/
void total_queue_size(struct queue *Q)
{
  struct queueNode *current = malloc(sizeof(struct queueNode));
  current = Q->Head->next; /*since head is sentinel node containing garbage*/
  int curr_queue_size = 0;
  while (current != NULL)
  {
    total_queues_size++;
    curr_queue_size++;
    current = current->next;
  }
  if (curr_queue_size != (thread_count * 2) && after_delete == 0)
  {
    printf("\n ERROR FOUND A QUEUE OF UNEXPECTED SIZE ! \n");
  }
  else if (curr_queue_size != (thread_count) && after_delete == 1)
  {
    printf("\n ERROR FOUND A QUEUE OF UNEXPECTED SIZE ! \n");
  }
}

/*checks queues keysum*/
void *check_queues_keysum()
{

  struct concurrent_queue_list *current = malloc(sizeof(struct concurrent_queue_list));
  current = head_queue;
  int i = 0;
  for (i = 0; i < thread_count / 2; i++)
  {
    total_queue_keysum(current->queue);
    current = current->next;
  }

  if (thread_count * thread_count * (thread_count - 1) * (thread_count + 1) / 2 == keysum_queues)
  {
    green();
    printf("\n queues Total keysum check passed (expected:%d , found: %d) \n", thread_count * thread_count * (thread_count - 1) * (thread_count + 1) / 2, keysum_queues);
    reset();
  }
  else
  {
    red();
    printf("\n queues Total keysum check ERROR (expected:%d , found: %d) \n", thread_count * thread_count * (thread_count - 1) * (thread_count + 1) / 2, keysum_queues);
    reset();
  }
  pthread_barrier_wait(&third_barrier);
}

/*calculates queues keysum*/
void total_queue_keysum(struct queue *Q)
{
  struct queueNode *current = malloc(sizeof(struct queueNode));
  current = Q->Head->next; /*since head is sentinel node containing garbage*/

  while (current != NULL)
  {
    keysum_queues = keysum_queues + (current->songID);
    current = current->next;
  }
}

/*checks tree keysum*/
void *total_keysum_check()
{
  keysum(root);
  if (thread_count * thread_count * (thread_count - 1) * (thread_count + 1) / 2 == keysum_n)
  {
    green();
    printf("\nTotal keysum check passed (expected:%d , found: %d) \n", thread_count * thread_count * (thread_count - 1) * (thread_count + 1) / 2, keysum_n);
    reset();
  }
  else
  {
    red();
    printf("\nTotal keysum check ERROR (expected:%d , found: %d) \n", thread_count * thread_count * (thread_count - 1) * (thread_count + 1) / 2, keysum_n);
    reset();
  }
}

/*calculates tree keysum*/
void keysum(struct node *root)
{

  if (root != NULL)
  {
    if (is_2_node(root))
    {
      keysum(root->child1);
      keysum_n = keysum_n + root->key1;
      ;
      keysum(root->child2);
    }
    else if (is_3_node(root))
    {
      keysum(root->child1);
      keysum_n = keysum_n + root->key1;
      keysum(root->child2);
      keysum_n = keysum_n + root->key2;
      keysum(root->child3);
    }
    else
    {
      keysum(root->child1);
      keysum_n = keysum_n + root->key1;
      keysum(root->child2);
      keysum_n = keysum_n + root->key2;
      keysum(root->child3);
      keysum_n = keysum_n + root->key3;
      keysum(root->child4);
    }
  }
}

/*ERROR SIZE AND KEYSUM CHECKING FUNCTIONS END*/
/*----------------------------------------------------------------*/

/*----------------------------------------------------------------*/
/*2-3-4 TREE FUNCTIONS START*/

/*creates and inits a new tree node*/
struct node *new_node(int key)
{
  struct node *new;
  new = malloc(sizeof(struct node));
  new->key1 = key;
  new->key2 = MARKED_VALUE;
  new->key3 = MARKED_VALUE;
  new->child1 = NULL;
  new->child2 = NULL;
  new->child3 = NULL;
  new->child4 = NULL;
  new->parent = NULL;

  pthread_mutex_init(&(new->lock), NULL);
  return new;
}

/*returns true if node is 2node false otherwise*/
int is_2_node(struct node *node)
{
  /*if it contains only one key it is a 2 node*/

  if (node->key2 == MARKED_VALUE)
  {
    return 1;
  }
  return 0;
}

/*returns true if node is 3node false otherwise*/
int is_3_node(struct node *node)
{
  /*if it contains only 2 keys it is a 3 node*/
  if (node->key3 == MARKED_VALUE && node->key2 != MARKED_VALUE)
  {
    return 1;
  }
  return 0;
}

/*returns true if node is leaf false otherwise*/
int is_leaf(struct node *node)
{
  if (node == NULL)
  {
    return 0;
  }
  /*if it doesnt have the leftmost child then the node is a leaf*/
  if (node->child1 == NULL)
  {
    return 1;
  }
  return 0;
}

/*sorts the keys to be placed correctly in tree node*/
void sort_keys(struct node *node)
{
  /*sorting the keys when inserted in a node*/
  if (is_2_node(node))
  {
    return;
  }
  else if (is_3_node(node))
  {
    if (node->key1 > node->key2)
    {
      int temp = node->key1;
      node->key1 = node->key2;
      node->key2 = temp;
    }
  }
  else
  {
    if (node->key1 > node->key3)
    {
      int temp1 = node->key1;
      node->key1 = node->key3;
      node->key3 = node->key2;
      node->key2 = temp1;
    }
    else if (node->key2 > node->key3)
    {
      int temp1 = node->key2;
      node->key2 = node->key3;
      node->key3 = temp1;
    }
  }
}

/*returns child position of the node, useful for deletion */
int find_which_child_am_I(struct node *node)
{
  if (node->parent->child1 == node)
  {
    return 1;
  }
  else if (node->parent->child2 == node)
  {
    return 2;
  }
  else if (node->parent->child3 == node)
  {
    return 3;
  }
  else
  {
    return 4;
  }
}

/*since the tree isnt modified in any way during the concurrent searches
there is in reality no need for locks which reduce the performance*/
int search(int key, struct node *node)
{
  struct node *current = node;
  //lock(current);
  while (1)
  {
    if (current == NULL)
    {
      //unlock(current->parent);
      return 0;
    }
    if (is_2_node(current))
    {
      if (is_leaf(current))
      {
        if (key == current->key1)
        {
          //unlock(current);
          //unlock(current -> parent);
          return 1;
        }
        else
        {
          /* reached leaf , not found
          so not in tree */
          // unlock(current);
          // unlock(current -> parent);
          return 0;
        }
      }
      else
      {
        if (key == current->key1)
        {
          // unlock(current);
          // unlock(current -> parent);
          return 1;
        }
        if (key > current->key1)
        {
          // unlock(current -> parent);
          //lock(current -> child2);
          current = current->child2;
        }
        else
        {
          // unlock(current -> parent);
          // lock(current -> child1);
          current = current->child1;
        }
      }
    }
    else if (is_3_node(current))
    {
      if (is_leaf(current))
      {
        if (key == current->key1 || key == current->key2)
        {
          // unlock(current);
          //unlock(current -> parent);
          return 1;
        }
        else
        {
          // unlock(current);
          // unlock(current -> parent);
          return 0;
        }
      }
      else
      {
        if (key == current->key1 || key == current->key2)
        {
          // unlock(current);
          // unlock(current -> parent);
          return 1;
        }
        if (key < current->key1)
        {
          //unlock(current -> parent);
          // lock(current -> child1);
          current = current->child1;
        }
        else if (key > current->key1 && key < current->key2)
        {
          //unlock(current -> parent);
          //lock(current -> child2);
          current = current->child2;
        }
        else
        {
          // unlock(current -> parent);
          // lock(current -> child3);
          current = current->child3;
        }
      }
    }
    else
    {
      if (is_leaf(current))
      {
        if (key == current->key1 || key == current->key2 ||
            key == current->key3)
        {
          // unlock(current);
          // unlock(current -> parent);
          return 1;
        }
        else
        {
          // unlock(current);
          // unlock(current -> parent);
          return 0;
        }
      }
      else
      {
        if (key == current->key1 || key == current->key2 ||
            key == current->key3)
        {
          // unlock(current);
          //unlock(current -> parent);
          return 1;
        }
        if (key < current->key1)
        {
          // unlock(current -> parent);
          //lock(current -> child1);
          current = current->child1;
        }
        else if (key > current->key1 && key < current->key2)
        {
          //unlock(current -> parent);
          // lock(current -> child2);
          current = current->child2;
        }
        else if (key > current->key2 && key < current->key3)
        {
          // unlock(current -> parent);
          // lock(current -> child3);
          current = current->child3;
        }
        else
        {
          //unlock(current -> parent);
          //lock(current -> child4);
          current = current->child4;
        }
      }
    }
  }
}

/*Insertion of 2-3-4 tree*/
void BSTInsert(int key)
{

  //Debug_print_tree();
  pthread_mutex_lock(&(root->lock));

  int depth = -1;
  /*root is empty I was the first insert in tree so
  create root with the value I wanted to insert*/
  if (root->key1 == MARKED_VALUE)
  {
    root->key1 = key;
    pthread_mutex_unlock(&(root->lock));
    return;
  }

  struct node *current = root;
  /*if smthng went really wrong*/
  if (current->parent != NULL)
  {
    pthread_mutex_unlock(&(current->lock));
    BSTInsert(key);
    return;
  }

  while (current != NULL)
  {
    /*used for debugging*/
    depth++;

    /*We encounter a 4 node try to change it*/
    if (!is_2_node(current) && !is_3_node(current))
    {

      /*is root and 4 node*/
      if (current->parent == NULL)
      {
        struct node *temp = new_node(MARKED_VALUE);
        temp->key1 = root->key1;
        temp->key2 = root->key2;
        temp->key3 = root->key3;
        temp->child1 = root->child1;
        temp->child2 = root->child2;
        temp->child3 = root->child3;
        temp->child4 = root->child4;
        //temp->lock=root->lock;

        root->key1 = root->key2;
        root->key2 = MARKED_VALUE;
        root->key3 = MARKED_VALUE;

        root->child1 = new_node(temp->key1);
        root->child2 = new_node(temp->key3);
        root->child1->parent = root;
        root->child2->parent = root;
        root->child1->child1 = temp->child1;
        if (root->child1->child1 != NULL)
        {
          root->child1->child1->parent = root->child1;
        }
        root->child1->child2 = temp->child2;
        if (root->child1->child2 != NULL)
        {
          root->child1->child2->parent = root->child1;
        }
        root->child2->child1 = temp->child3;
        if (root->child2->child1 != NULL)
        {
          root->child2->child1->parent = root->child2;
        }
        root->child2->child2 = temp->child4;
        if (root->child2->child2 != NULL)
        {
          root->child2->child2->parent = root->child2;
        }
        root->child3 = NULL;
        root->child4 = NULL;

        pthread_mutex_unlock(&(root->lock));
        /*lets try again from the top*/
        BSTInsert(key);
        return;
      }
      else if (is_2_node(current->parent))
      {
        if (current == current->parent->child1)
        {

          current->parent->key2 = current->key2;
          sort_keys(current->parent);

          current->parent->child3 = current->parent->child2;
          current->parent->child1 = new_node(current->key1);
          current->parent->child2 = new_node(current->key3);

          current->parent->child1->child1 = current->child1;
          current->parent->child1->child2 = current->child2;
          current->parent->child2->child1 = current->child3;
          current->parent->child2->child2 = current->child4;
          if (current->parent->child1->child1 != NULL)
          {
            current->parent->child1->child1->parent = current->parent->child1;
          }
          if (current->parent->child1->child2 != NULL)
          {
            current->parent->child1->child2->parent = current->parent->child1;
          }
          if (current->parent->child2->child1 != NULL)
          {
            current->parent->child2->child1->parent = current->parent->child2;
          }
          if (current->parent->child2->child2 != NULL)
          {
            current->parent->child2->child2->parent = current->parent->child2;
          }
          current->parent->child1->parent = current->parent;
          current->parent->child2->parent = current->parent;

          pthread_mutex_unlock(&(current->lock));
          pthread_mutex_unlock(&(current->parent->lock));
          /*lets try again from the top*/
          BSTInsert(key);
          return;
        }
        else
        {
          current->parent->key2 = current->key2;
          sort_keys(current->parent);

          current->parent->child2 = new_node(current->key1);
          current->parent->child3 = new_node(current->key3);

          current->parent->child2->child1 = current->child1;
          current->parent->child2->child2 = current->child2;
          current->parent->child3->child1 = current->child3;
          current->parent->child3->child2 = current->child4;

          if (current->parent->child3->child1 != NULL)
          {
            current->parent->child3->child1->parent = current->parent->child3;
          }
          if (current->parent->child3->child2 != NULL)
          {
            current->parent->child3->child2->parent = current->parent->child3;
          }
          if (current->parent->child2->child1 != NULL)
          {
            current->parent->child2->child1->parent = current->parent->child2;
          }
          if (current->parent->child2->child2 != NULL)
          {
            current->parent->child2->child2->parent = current->parent->child2;
          }

          current->parent->child2->parent = current->parent;
          current->parent->child3->parent = current->parent;

          pthread_mutex_unlock(&(current->lock));
          pthread_mutex_unlock(&(current->parent->lock));
          /*lets try again from the top*/
          BSTInsert(key);
          return;
        }
      }
      else if (is_3_node(current->parent))
      {

        if (current == current->parent->child1)
        {

          current->parent->key3 = current->key2;
          sort_keys(current->parent);

          current->parent->child4 = current->parent->child3;
          current->parent->child3 = current->parent->child2;

          current->parent->child1 = new_node(current->key1);
          current->parent->child2 = new_node(current->key3);

          current->parent->child1->child1 = current->child1;
          current->parent->child1->child2 = current->child2;
          current->parent->child2->child1 = current->child3;
          current->parent->child2->child2 = current->child4;

          if (current->parent->child1->child1 != NULL)
          {
            current->parent->child1->child1->parent = current->parent->child1;
          }
          if (current->parent->child1->child2 != NULL)
          {
            current->parent->child1->child2->parent = current->parent->child1;
          }
          if (current->parent->child2->child1 != NULL)
          {
            current->parent->child2->child1->parent = current->parent->child2;
          }
          if (current->parent->child2->child2 != NULL)
          {
            current->parent->child2->child2->parent = current->parent->child2;
          }

          current->parent->child1->parent = current->parent;
          current->parent->child2->parent = current->parent;

          pthread_mutex_unlock(&(current->lock));
          pthread_mutex_unlock(&(current->parent->lock));
          /*lets try again from the top*/
          BSTInsert(key);
          return;
        }
        else if (current == current->parent->child2)
        {

          current->parent->key3 = current->key2;
          sort_keys(current->parent);

          current->parent->child4 = current->parent->child3;

          current->parent->child2 = new_node(current->key1);
          current->parent->child3 = new_node(current->key3);

          current->parent->child2->child1 = current->child1;
          current->parent->child2->child2 = current->child2;
          current->parent->child3->child1 = current->child3;
          current->parent->child3->child2 = current->child4;

          if (current->parent->child3->child1 != NULL)
          {
            current->parent->child3->child1->parent = current->parent->child3;
          }
          if (current->parent->child3->child2 != NULL)
          {
            current->parent->child3->child2->parent = current->parent->child3;
          }
          if (current->parent->child2->child1 != NULL)
          {
            current->parent->child2->child1->parent = current->parent->child2;
          }
          if (current->parent->child2->child2 != NULL)
          {
            current->parent->child2->child2->parent = current->parent->child2;
          }

          current->parent->child2->parent = current->parent;
          current->parent->child3->parent = current->parent;

          pthread_mutex_unlock(&(current->lock));
          pthread_mutex_unlock(&(current->parent->lock));
          /*lets try again from the top*/
          BSTInsert(key);
          return;
        }
        else
        {
          current->parent->key3 = current->key2;
          sort_keys(current->parent);

          current->parent->child3 = new_node(current->key1);
          current->parent->child4 = new_node(current->key3);

          current->parent->child3->child1 = current->child1;
          current->parent->child3->child2 = current->child2;
          current->parent->child4->child1 = current->child3;
          current->parent->child4->child2 = current->child4;

          if (current->parent->child3->child1 != NULL)
          {
            current->parent->child3->child1->parent = current->parent->child3;
          }
          if (current->parent->child3->child2 != NULL)
          {
            current->parent->child3->child2->parent = current->parent->child3;
          }
          if (current->parent->child4->child1 != NULL)
          {
            current->parent->child4->child1->parent = current->parent->child4;
          }
          if (current->parent->child4->child2 != NULL)
          {
            current->parent->child4->child2->parent = current->parent->child4;
          }

          current->parent->child3->parent = current->parent;
          current->parent->child4->parent = current->parent;
          pthread_mutex_unlock(&(current->lock));
          pthread_mutex_unlock(&(current->parent->lock));
          /*lets try again from the top*/
          BSTInsert(key);
          return;
        }
      }
    }
    else if (is_leaf(current))
    {
      //printf("\nleaf node encountered!!\n");
      if (is_2_node(current))
      {
        current->key2 = key;
        sort_keys(current);
        pthread_mutex_unlock(&(current->lock));
        if (current->parent != NULL)
        {
          pthread_mutex_unlock(&(current->parent->lock));
        }
        return;
      }
      else if (is_3_node(current))
      {
        current->key3 = key;
        sort_keys(current);
        pthread_mutex_unlock(&(current->lock));
        if (current->parent != NULL)
        {
          pthread_mutex_unlock(&(current->parent->lock));
        }
        return;
      }
    }
    else
    {
      if (is_2_node(current))
      {
        // printf("\n2 node encountered!!\n");
        if (key > current->key1)
        {
          pthread_mutex_lock(&(current->child2->lock));
          if (current->parent != NULL)
          {
            pthread_mutex_unlock(&(current->parent->lock));
          }
          current = current->child2;
        }
        else
        {
          pthread_mutex_lock(&(current->child1->lock));
          if (current->parent != NULL)
          {
            pthread_mutex_unlock(&(current->parent->lock));
          }
          current = current->child1;
        }
      }
      else if (is_3_node(current))
      {
        //printf("\n3 node encountered!!\n");
        if (key < current->key1)
        {
          pthread_mutex_lock(&(current->child1->lock));
          if (current->parent != NULL)
          {
            pthread_mutex_unlock(&(current->parent->lock));
          }
          current = current->child1;
        }
        else if (key > current->key1 && key < current->key2)
        {
          pthread_mutex_lock(&(current->child2->lock));
          if (current->parent != NULL)
          {
            pthread_mutex_unlock(&(current->parent->lock));
          }
          current = current->child2;
        }
        else
        {
          pthread_mutex_lock(&(current->child3->lock));
          if (current->parent != NULL)
          {
            pthread_mutex_unlock(&(current->parent->lock));
          }
          current = current->child3;
        }
      }
    }
  }
}

/*used in delete to convert 2node to 3 node by stealing a sibling key*/
/*rotation*/
void steal_from_right(struct node *curr, struct node *sibling)
{
  /*steal his child*/
  curr->child3 = sibling->child1;
  if (curr->child3 != NULL)
  {
    curr->child3->parent = curr;
  }
  sibling->child1 = sibling->child2;
  sibling->child2 = sibling->child3;
  sibling->child3 = sibling->child4;
  sibling->child4 = NULL;

  if (is_2_node(curr->parent))
  {
    curr->key2 = curr->parent->key1;
    curr->parent->key1 = sibling->key1;
  }
  else if (is_3_node(curr->parent))
  {
    if (find_which_child_am_I(curr) == 1)
    {
      curr->key2 = curr->parent->key1;
      curr->parent->key1 = sibling->key1;
    }
    else
    {
      curr->key2 = curr->parent->key2;
      curr->parent->key2 = sibling->key1;
    }
  }
  else
  {
    if (find_which_child_am_I(curr) == 1)
    {
      curr->key2 = curr->parent->key1;
      curr->parent->key1 = sibling->key1;
    }
    else if (find_which_child_am_I(curr) == 2)
    {
      curr->key2 = curr->parent->key2;
      curr->parent->key2 = sibling->key1;
    }
    else
    {
      curr->key2 = curr->parent->key3;
      curr->parent->key3 = sibling->key1;
    }
  }

  sibling->key1 = sibling->key2;
  sibling->key2 = sibling->key3;
  sibling->key3 = MARKED_VALUE;

  sort_keys(curr);
  sort_keys(sibling);
}

/*used in delete to convert 2node to 3 node by stealing a sibling key*/
/*rotation*/
void steal_from_left(struct node *curr, struct node *sibling)
{
  /*steal his child*/
  curr->child3 = curr->child2;
  curr->child2 = curr->child1;

  if (find_which_child_am_I(curr) == 2)
  {
    curr->key2 = curr->parent->key1;
    if (is_3_node(sibling))
    {
      curr->child1 = sibling->child3;
      if (curr->child1 != NULL)
      {
        curr->child1->parent = curr;
      }
      sibling->child3 = NULL;
      curr->parent->key1 = sibling->key2;
      sibling->key2 = MARKED_VALUE;
    }
    else
    {
      curr->child1 = sibling->child4;
      if (curr->child1 != NULL)
      {
        curr->child1->parent = curr;
      }
      sibling->child4 = NULL;
      curr->parent->key1 = sibling->key3;
      sibling->key3 = MARKED_VALUE;
    }
  }
  else if (find_which_child_am_I(curr) == 3)
  {
    curr->key2 = curr->parent->key2;
    if (is_3_node(sibling))
    {
      curr->child1 = sibling->child3;
      if (curr->child1 != NULL)
      {
        curr->child1->parent = curr;
      }
      sibling->child3 = NULL;
      curr->parent->key2 = sibling->key2;
      sibling->key2 = MARKED_VALUE;
    }
    else
    {
      curr->child1 = sibling->child4;
      if (curr->child1 != NULL)
      {
        curr->child1->parent = curr;
      }
      sibling->child4 = NULL;
      curr->parent->key2 = sibling->key3;
      sibling->key3 = MARKED_VALUE;
    }
  }
  else if (find_which_child_am_I(curr) == 4)
  {
    curr->key2 = curr->parent->key3;
    if (is_3_node(sibling))
    {
      curr->child1 = sibling->child3;
      if (curr->child1 != NULL)
      {
        curr->child1->parent = curr;
      }
      sibling->child3 = NULL;
      curr->parent->key3 = sibling->key2;
      sibling->key2 = MARKED_VALUE;
    }
    else
    {
      curr->child1 = sibling->child4;
      if (curr->child1 != NULL)
      {
        curr->child1->parent = curr;
      }
      sibling->child4 = NULL;
      curr->parent->key3 = sibling->key3;
      sibling->key3 = MARKED_VALUE;
    }
  }

  sort_keys(curr);
  sort_keys(sibling);
}

/*used in delete to convert 2node to 4 node by mergin with a sibling*/
/*Merge/Fusion*/
void merge(struct node *curr, struct node *sibling)
{
  /*if I am the leftmost child I can only merge
  with the second child*/
  if (find_which_child_am_I(curr) == 1)
  {
    curr->key2 = curr->parent->key1;
    curr->parent->key1 = curr->parent->key2;
    curr->parent->key2 = curr->parent->key3;
    curr->parent->key3 = MARKED_VALUE;
    curr->key3 = sibling->key1;
    curr->child3 = sibling->child1;
    if (curr->child3 != NULL)
    {
      curr->child3->parent = curr;
    }
    curr->child4 = sibling->child2;
    if (curr->child4 != NULL)
    {
      curr->child4->parent = curr;
    }
    curr->parent->child2 = curr->parent->child3;
    curr->parent->child3 = curr->parent->child4;
    curr->parent->child4 = NULL;
  }
  else if (find_which_child_am_I(curr) == 2 && find_which_child_am_I(sibling) == 1)
  {
    sibling->key2 = sibling->parent->key1;
    sibling->parent->key1 = sibling->parent->key2;
    sibling->parent->key2 = sibling->parent->key3;
    sibling->parent->key3 = MARKED_VALUE;
    sibling->key3 = curr->key1;
    sibling->child3 = curr->child1;
    if (sibling->child3 != NULL)
    {
      sibling->child3->parent = sibling;
    }
    sibling->child4 = curr->child2;
    if (sibling->child4 != NULL)
    {
      sibling->child4->parent = sibling;
    }
    sibling->parent->child2 = curr->parent->child3;
    sibling->parent->child3 = curr->parent->child4;
    sibling->parent->child4 = NULL;
  }
  else if (find_which_child_am_I(curr) == 2 && find_which_child_am_I(sibling) == 3)
  {
    curr->key2 = curr->parent->key2;
    curr->parent->key2 = curr->parent->key3;
    curr->parent->key3 = MARKED_VALUE;
    curr->key3 = sibling->key1;
    curr->child3 = sibling->child1;
    if (curr->child3 != NULL)
    {
      curr->child3->parent = curr;
    }
    curr->child4 = sibling->child2;
    if (curr->child4 != NULL)
    {
      curr->child4->parent = curr;
    }
    curr->parent->child3 = curr->parent->child4;
    curr->parent->child4 = NULL;
  }
  else if (find_which_child_am_I(curr) == 3 && find_which_child_am_I(sibling) == 2)
  {
    sibling->key2 = sibling->parent->key2;
    sibling->parent->key2 = sibling->parent->key3;
    sibling->parent->key3 = MARKED_VALUE;
    sibling->key3 = curr->key1;
    sibling->child3 = curr->child1;
    if (sibling->child3 != NULL)
    {
      sibling->child3->parent = sibling;
    }
    sibling->child4 = curr->child2;
    if (sibling->child4 != NULL)
    {
      sibling->child4->parent = sibling;
    }
    sibling->parent->child3 = sibling->parent->child4;
    sibling->parent->child4 = NULL;
  }
  else if (find_which_child_am_I(curr) == 3 && find_which_child_am_I(sibling) == 4)
  {
    curr->key2 = curr->parent->key3;
    curr->parent->key3 = MARKED_VALUE;
    curr->key3 = sibling->key1;
    curr->child3 = sibling->child1;
    if (curr->child3 != NULL)
    {
      curr->child3->parent = curr;
    }
    curr->child4 = sibling->child2;
    if (curr->child4 != NULL)
    {
      curr->child4->parent = curr;
    }
    curr->parent->child4 = NULL;
  }
  else
  {
    sibling->key2 = sibling->parent->key3;
    sibling->parent->key3 = MARKED_VALUE;
    sibling->key3 = curr->key1;
    sibling->child3 = curr->child1;
    if (sibling->child3 != NULL)
    {
      sibling->child3->parent = sibling;
    }
    sibling->child4 = curr->child2;
    if (sibling->child4 != NULL)
    {
      sibling->child4->parent = sibling;
    }
    sibling->parent->child4 = NULL;
  }

  sort_keys(curr);
  sort_keys(sibling);
}

/*used in delete if we cant convert the 2 node any other way*/
/*cant merge because parent is 2-node, cant rotate cause siblings are 2 nodes*/
/*Only happens if parent is 2-node root*/
/*Tree shrink*/
void tree_shrink()
{
  //printf("\n LET ME SEE IF I ERROR CAUSE OF THIS!!! \n");
  root->key2 = root->key1;
  root->key1 = root->child1->key1;
  root->key3 = root->child2->key1;
  struct node *temp1 = new_node(MARKED_VALUE);
  struct node *temp2 = new_node(MARKED_VALUE);
  temp1 = root->child1;
  temp2 = root->child2;
  root->child1 = temp1->child1;
  if (root->child1 != NULL)
  {
    root->child1->parent = root;
  }
  root->child2 = temp1->child2;
  if (root->child2 != NULL)
  {
    root->child2->parent = root;
  }
  root->child3 = temp2->child1;
  if (root->child3 != NULL)
  {
    root->child3->parent = root;
  }
  root->child4 = temp2->child2;
  if (root->child4 != NULL)
  {
    root->child4->parent = root;
  }
}

/*returns true if key is in curr node*/
int key_in_node(struct node *curr, int key)
{
  if (curr->key1 == key || curr->key2 == key || curr->key3 == key)
  {
    return 1;
  }
  return 0;
}

/*swaps key from marked node to curr leaf node*/
void swap(struct node *curr, struct node *marked, int key)
{
  int temp;
  if (key == marked->key1)
  {
    marked->key1 = curr->key1;
  }
  else if (key == marked->key2)
  {
    marked->key2 = curr->key1;
  }
  else if (key == marked->key3)
  {
    marked->key3 = curr->key1;
  }
  else
  {
    return;
  }

  curr->key1 = curr->key2;
  curr->key2 = curr->key3;
  curr->key3 = MARKED_VALUE;
  sort_keys(curr);
  sort_keys(marked);
}

/*Deletes key from 2-3-4 tree*/
int BSTDelete(int key)
{
  int locks = 0;
  //printf("\nWhat am I trying to delete?? %d ", key);
  //printf("\nDid I fail to lock root?? ->");
  pthread_mutex_lock(&(root->lock));
  locks++;
  //printf("Successfully got root");
  int depth = -1;
  if (root->key1 == MARKED_VALUE)
  {
    /*tree is empty return failure*/
    pthread_mutex_unlock(&(root->lock));
    return 0;
  }

  struct node *current = root;
  struct node *marked = NULL;
  //printf("\n--------------------------\n");
  //Debug_print_tree(root, 0);
  while (current != NULL)
  {
    depth++;
    //printf("\nLet me see in which depth I am stuck %d key:%d", depth, key);

    /*turning every 2 node into 3 or 4 node*/
    if (is_2_node(current) && (current->parent != NULL))
    {
      //printf("\ntrying to turn 2node into 3-4 node! key:%d", key);
      /*i am leftmost child*/
      if (find_which_child_am_I(current) == 1)
      {
        /*we can steal from right sibling*/
        if (!is_2_node(current->parent->child2))
        {
          // printf("\nsteal from right key:%d", key);
          pthread_mutex_lock(&current->parent->child2->lock);
          steal_from_right(current, current->parent->child2);
          pthread_mutex_unlock(&current->parent->child2->lock);
        }
        /*we can merge from parent*/
        else if (!is_2_node(current->parent))
        {
          // printf("\n merge:%d", key);
          pthread_mutex_lock(&current->parent->child2->lock);
          merge(current, current->parent->child2);
          pthread_mutex_unlock(&current->parent->child2->lock);
        }
        else
        {
          //printf("\nTree shrink:%d", key);
          pthread_mutex_lock(&current->parent->child2->lock);
          tree_shrink();
          pthread_mutex_unlock(&current->parent->child2->lock);
        }
      }
      else if (find_which_child_am_I(current) == 2)
      {
        /*we can steal from left sibling*/
        if (!is_2_node(current->parent->child1))
        {
          // printf("\nsteal from left key:%d", key);
          pthread_mutex_lock(&current->parent->child1->lock);
          steal_from_left(current, current->parent->child1);
          pthread_mutex_unlock(&current->parent->child1->lock);
        }
        /*we can steal from right sibling*/
        else if (current->parent->child3 != NULL && !is_2_node(current->parent->child3))
        {
          // printf("\nsteal from right key:%d", key);
          pthread_mutex_lock(&current->parent->child3->lock);
          steal_from_right(current, current->parent->child3);
          pthread_mutex_unlock(&current->parent->child3->lock);
        }
        else if (!is_2_node(current->parent))
        {
          //printf("\nmerge");
          pthread_mutex_lock(&current->parent->child1->lock);
          merge(current, current->parent->child1);
          pthread_mutex_unlock(&current->parent->child1->lock);
        }
        else
        {
          //printf("\nTree shrink:%d", key);
          tree_shrink();
        }
      }
      else if (find_which_child_am_I(current) == 3)
      {
        /*steal from left sibling*/
        if (!is_2_node(current->parent->child2))
        {
          // printf("\nsteal from left key:%d", key);
          pthread_mutex_lock(&current->parent->child2->lock);
          steal_from_left(current, current->parent->child2);
          pthread_mutex_unlock(&current->parent->child2->lock);
        }
        /*steal from right sibling*/
        else if (current->parent->child4 != NULL && !is_2_node(current->parent->child4))
        {
          //printf("\nsteal from right key:%d", key);
          pthread_mutex_lock(&current->parent->child4->lock);
          steal_from_right(current, current->parent->child4);
          pthread_mutex_unlock(&current->parent->child4->lock);
        }
        else
        {
          //printf("\nmerge:%d", key);
          pthread_mutex_lock(&current->parent->child2->lock);
          merge(current, current->parent->child2);
          pthread_mutex_unlock(&current->parent->child2->lock);
        }
      }
      else
      {
        /*steal from left sibling*/
        if (!is_2_node(current->parent->child3))
        {
          //printf("\nsteal from left key:%d", key);
          pthread_mutex_lock(&current->parent->child3->lock);
          steal_from_left(current, current->parent->child3);
          pthread_mutex_unlock(&current->parent->child3->lock);
        }
        else
        {
          //printf("\nmerge:%d", key);
          pthread_mutex_lock(&current->parent->child3->lock);
          merge(current, current->parent->child3);
          pthread_mutex_unlock(&current->parent->child3->lock);
        }
      }

      /*make sure no mistake happens by trying again from the top*/
      if (is_leaf(current))
      {
        if (marked != NULL && marked != (current->parent))
        {
          locks--;
          pthread_mutex_unlock(&marked->lock);
        }
        pthread_mutex_unlock(&current->lock);
        locks--;
        pthread_mutex_unlock(&current->parent->lock);
        locks--;

        usleep(rand() % ((300 - 50 + 1)) + 50);
        BSTDelete(key);
      }
    }

    /*key not in tree failed to delete */
    if (is_leaf(current) && !key_in_node(current, key) && marked == NULL)
    {
      //printf("\ntkey not in tree failed to delete key:%d", key);
      pthread_mutex_unlock(&(current->lock));
      locks--;
      if (current->parent != NULL)
      {
        pthread_mutex_unlock(&(current->parent->lock));
        locks--;
      }
      /*this should not happen though so try to delete again */

      //printf("before I go I have %d locks",locks);
      //BSTDelete(key);
      return 0;
    }
    /* key is in tree , reached pred or succ node , swap */
    else if (is_leaf(current) && !key_in_node(current, key) && marked != NULL)
    {
      //printf("\ntkey is in tree , reached pred or succ node , swap key:%d", key);
      swap(current, marked, key); /* swap and remove the correct elements of current node and marked node */
      sort_keys(current);
      pthread_mutex_unlock(&(current->lock));
      locks--;
      pthread_mutex_unlock(&(marked->lock));
      locks--;
      if (current->parent != marked)
      {

        pthread_mutex_unlock(&(current->parent->lock));
        locks--;
      }
      //printf("before I go I have %d locks",locks);
      return 1;
    }
    /* key is in node and node is leaf */
    else if (is_leaf(current) && key_in_node(current, key))
    {
      //printf("\nkey is in node and node is leaf key:%d", key);
      if (key == current->key1)
      {
        current->key1 = current->key2;
        current->key2 = current->key3;
        current->key3 = MARKED_VALUE;
      }
      else if (key == current->key2)
      {
        current->key2 = current->key3;
        current->key3 = MARKED_VALUE;
      }
      else if (key == current->key3)
      {
        current->key3 = MARKED_VALUE;
      }
      sort_keys(current);
      pthread_mutex_unlock(&(current->lock));
      locks--;
      if (current->parent != NULL)
      {
        pthread_mutex_unlock(&(current->parent->lock));
        locks--;
      }
      if (marked != NULL)
      {
        pthread_mutex_unlock(&(marked->lock));
        locks--;
      }
      return 1;
    }
    /* key is in node and node is not leaf */
    else if (key_in_node(current, key) && marked == NULL)
    {

      marked = current;
      if (key == current->key1)
      {
        pthread_mutex_lock(&(current->child2->lock));
        locks++;
        if (current->parent != NULL)
        {
          pthread_mutex_unlock(&(current->parent->lock));
          locks--;
        }
        current = current->child2;
      }
      else if (key == current->key2)
      {
        pthread_mutex_lock(&(current->child3->lock));
        locks++;
        if (current->parent != NULL)
        {
          pthread_mutex_unlock(&(current->parent->lock));
          locks--;
        }
        current = current->child3;
      }
      else if (key == current->key3)
      {
        pthread_mutex_lock(&(current->child4->lock));
        locks++;
        if (current->parent != NULL)
        {
          pthread_mutex_unlock(&(current->parent->lock));
          locks--;
        }
        current = current->child4;
      }
    }
    /*have not found node containing key yet*/
    else if (marked == NULL && !is_leaf(current))
    {
      //printf("\nhave not found node containing key yet key:%d",key);
      if (is_2_node(current))
      {
        // printf("\n2 node encountered!!\n");
        if (key > current->key1)
        {

          pthread_mutex_lock(&(current->child2->lock));
          locks++;
          if (current->parent != NULL)
          {
            pthread_mutex_unlock(&(current->parent->lock));
            locks--;
          }

          current = current->child2;
        }
        else
        {

          pthread_mutex_lock(&(current->child1->lock));
          locks++;
          if (current->parent != NULL)
          {
            pthread_mutex_unlock(&(current->parent->lock));
            locks--;
          }
          current = current->child1;
        }
      }
      else if (is_3_node(current))
      {
        //printf("\n3 node encountered!!\n");
        if (key < current->key1)
        {
          pthread_mutex_lock(&(current->child1->lock));
          locks++;
          if (current->parent != NULL)
          {
            pthread_mutex_unlock(&(current->parent->lock));
            locks--;
          }
          current = current->child1;
        }
        else if (key > current->key1 && key < current->key2)
        {
          locks++;
          pthread_mutex_lock(&(current->child2->lock));
          if (current->parent != NULL)
          {
            pthread_mutex_unlock(&(current->parent->lock));
            locks--;
          }
          current = current->child2;
        }
        else
        {
          
          if(&current->child3->lock == NULL)
          {
            pthread_mutex_init(&current->child3->lock,NULL);
          }
          pthread_mutex_lock(&(current->child3->lock));
          locks++;
          if (current->parent != NULL)
          {
            pthread_mutex_unlock(&(current->parent->lock));
            locks--;
          }
          current = current->child3;
        }
      }
      else
      {
        sort_keys(current);
        if (key < current->key1)
        {

          pthread_mutex_lock(&(current->child1->lock));
          locks++;
          if (current->parent != NULL)
          {
            pthread_mutex_unlock(&(current->parent->lock));
            locks--;
          }
          current = current->child1;
        }
        else if (key > current->key1 && key < current->key2)
        {

          pthread_mutex_lock(&(current->child2->lock));
          locks++;
          if (current->parent != NULL)
          {
            pthread_mutex_unlock(&(current->parent->lock));
            locks--;
          }
          current = current->child2;
        }
        else if (key > current->key2 && key < current->key3)
        {

          pthread_mutex_lock(&(current->child3->lock));
          locks++;
          if (current->parent != NULL)
          {
            pthread_mutex_unlock(&(current->parent->lock));
            locks--;
          }
          current = current->child3;
        }
        else
        {
          if (current->child4 == NULL)
          {
            // printf("KATI EXEI PAEI POLU STRAVA!!!!!!!!!!!!");
          }
          pthread_mutex_lock(&(current->child4->lock));
          locks++;
          if (current->parent != NULL)
          {
            pthread_mutex_unlock(&(current->parent->lock));
            locks--;
          }
          current = current->child4;
        }
      }
    }
    else
    {

      pthread_mutex_lock(&(current->child1->lock));
      locks++;
      /* we dont unlock parent if it is the marked node */
      if (current->parent != marked)
      {
        pthread_mutex_unlock(&(current->parent->lock));
        locks--;
      }
      current = current->child1;
    }
  }
}

/*main thread of program creates all other threads 
and manages some global initializers*/
int main(int argc, char *argv[])
{

  if (argc != 3)
  {
    usage();
  }
  if (!strcmp(argv[1], "-t"))
  {
    /*read thread count from input*/
    thread_count = strtol(argv[2], NULL, 10);

    printf("Thread Count:%d \n", thread_count);

    /*Initialize barriers*/
    pthread_barrier_init(&first_barrier, NULL, thread_count + 1);
    pthread_barrier_init(&second_barrier, NULL, thread_count + 1);
    pthread_barrier_init(&third_barrier, NULL, 2);
    pthread_barrier_init(&fourth_barrier, NULL, thread_count + 1);

    /*Initialize root of 2-3-4 tree*/
    root = new_node(MARKED_VALUE);
    /*allocate space for head of concurrent queues list*/
    head_queue = malloc(sizeof(struct concurrent_queue_list));

    /*allocate space for shared list */
    sharedlist = malloc(sizeof(struct list));
    sharedlist->head = new_list_node(-1);
    sharedlist->tail = new_list_node(-1);
    sharedlist->head->next = sharedlist->tail;

    pthread_attr_init(&attr);
    /*create thread 0 and rest of the threads\song managers */
    head_s = malloc(sizeof(struct song_manager));
    initialize_song_manager(head_s, 0);
    make_song_managers_list(head_s, thread_count);
    printthreadList(head_s);

    /*begin tree insertion*/
    start_inserting_tree(head_s);
    /*wait for thread to finish inserting*/
    pthread_barrier_wait(&first_barrier);

    /*Do after insert tree checks*/
    pthread_create(&head_s->thread, &attr, check_tree_size_keysum, NULL);
    

    /*Begin inserting in queues*/
    start_inserting_queues(head_s);
    /*Wait for  queues inserts to finish*/
    pthread_barrier_wait(&second_barrier);

    /*check queues size after insert*/
    pthread_create(&head_s->thread, &attr, check_queues_size, NULL);
    /*wait for check to finish*/
    pthread_barrier_wait(&third_barrier);

    //print_queues(head_queue);
    //printsharedList();
    /*start deleting*/
    start_deleting(head_s);
    /*wait for deletes to finish*/
    pthread_barrier_wait(&fourth_barrier);

    //print_queues(head_queue);
    //printsharedList();
    /*perform after delete checks*/
    pthread_create(&head_s->thread, &attr, check_after_delete, NULL);
    

    //print_tree(root);

    /*program is done join every thread*/
    struct song_manager *current = head_s;
    while (current != NULL)
    {
      pthread_join(current->thread, NULL);
      current = current->next;
    }
  }
  return 0;
}
