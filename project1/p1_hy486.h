
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include <stdatomic.h>
#include <unistd.h>
#include <time.h>

/*no song with id 88888888,not enough threads 
so used instead of NULL for empty checking*/
#define MARKED_VALUE 88888888

/*struct of each data structure used*/

/*tree node*/
struct node
{
    int key1;
    int key2;
    int key3;
    struct node *child1;
    struct node *child2;
    struct node *child3;
    struct node *child4;
    struct node *parent;
    pthread_mutex_t lock;
};

struct queueNode
{
    int songID;
    struct queueNode *next;
};

struct queue
{
    struct queueNode *Head;
    struct queueNode *Tail;
};

struct concurrent_queue_list
{
    struct queue *queue;
    struct concurrent_queue_list *next;
};

struct song_manager
{
    pthread_t thread;
    int song_manager_id;
    struct song_manager *next;
};

struct listNode
{
    int songID;
    struct listNode *next;
    int marked; //for grads
    pthread_mutex_t lock;
};

struct list
{
    struct listNode *head;
    struct listNode *tail;
};

struct list *sharedlist;

/*Global Vars*/
static struct node *root;
static unsigned int thread_count = 0;
pthread_attr_t attr;
static struct concurrent_queue_list *head_queue;
struct song_manager *head_s;
static int lock_count = 0;
static int total_songs = 0;
static int total_queues_size = 0;
static unsigned int keysum_n = 0;
static unsigned int keysum_queues = 0;
int after_delete = 0;

/*DEBUG FUNCTIONS*/
void usage(void);
/*prints the contents of the entire tree, usefull for debugging*/
void print_tree(struct node *root);
/*prints certain levels of the tree used for debugging*/
void Debug_print_tree(struct node *node, int depth);
/*changes the text color to red */
void red ();
/*changes the text color to green */
void green();
/*resets the text color */
void reset();
/*used for testing if threads are made correctly*/
void printthreadList(struct song_manager *node);
/*used for testing contents of shared list*/
void printsharedList();
/*prints the contents of the entire tree, usefull for debugging*/
void print_tree(struct node *root);
/*prints certain levels of the tree in 2-3-4 format used for debugging*/
void Debug_print_tree(struct node *node, int depth);
/*prints every element of a queue*/
void print_queue_debug(struct queue *Q);
/*prints all queues*/
void print_queues(struct concurrent_queue_list *Q);

/*LIST FUNCTIONS START*/
/*shared list validate*/
int validate(struct listNode *pred, struct listNode *curr);
/*creates a new list node*/
struct listNode *new_list_node(int key);
/*insert key into shared lazy list*/
int list_insert(int key);

/*Queue FUNCTIONS START*/
/*make a new Queue*/
struct queue *make_queue();
/*initialiaze elements of Queue*/
void init(struct queue *Q);
/*Create a new list containing all queues*/
void make_queue_list(int M);
/*enq\insert value to queue Q*/
void enq(struct queue *Q, int value);
/*deq/delete value from queue Q*/
int deq(struct queue *Q, int *pvalue);

/*THREAD MANAGERS FUNCTIONS START*/

/*initialized a new song manager thread*/
void initialize_song_manager(struct song_manager *c, int id);
/*creates the list containing the song managers\users threads*/
void make_song_managers_list(struct song_manager *head, int count);
/*each thread begins inserting into 2-3-4 tree*/
void start_inserting_tree(struct song_manager *head);
/*each thread begins inserting into lock free queues*/
void start_inserting_queues(struct song_manager *head);
/*each thread begins the delete ops into 2-3-4 tree*/
void start_deleting(struct song_manager *head);
/*inserts songs into 2-3-4 tree based on songmanager_id and thread count*/
void *Song_generator(void *curr_thread);
/*inserts songs in queue in the way requested by the exercise*/
void *Song_user_queue_insert(void *curr_thread);
/*deqs from queues, deletes from tree and inserts in list*/
/*Because of the nature of the 2-3-4 tree delete from tree
and insert in shared list are done inside deq*/
void *Song_manager_delete(void *curr_thread);

/*ERROR SIZE AND KEYSUM CHECKING FUNCTIONS START*/

/*checks tree size after completion of insert*/
void *check_tree_size();
/*checks tree size after completion of delete*/
void *check_tree_size_after_delete();
/*the function called to calculate ammount of songs in tree*/
void Tree_Size_Check(struct node *root);
/*checks shared list size after completion of insert*/
void *check_list_size();
/*function used to calculate num of elements inside list*/
int sharedList_size();
/*checks queues size after completion of insert*/
void *check_queues_size();
/*checks queues size after completion of delete\deq*/
void *check_queues_size_after_delete();
/*calculates num of songs in each queue and displays error message
if queue of unexpected size is found*/
void total_queue_size(struct queue *Q);
/*checks queues keysum*/
void *check_queues_keysum();
/*calculates queues keysum*/
void total_queue_keysum(struct queue *Q);
/*checks tree keysum*/
void *total_keysum_check();
/*calculates tree keysum*/
void keysum(struct node *root);

/*2-3-4 TREE FUNCTIONS START*/

/*creates and inits a new tree node*/
struct node *new_node(int key);
/*returns true if node is 2node false otherwise*/
int is_2_node(struct node *node);
/*returns true if node is 3node false otherwise*/
int is_3_node(struct node *node);
/*returns true if node is leaf false otherwise*/
int is_leaf(struct node *node);
/*sorts the keys to be placed correctly in tree node*/
void sort_keys(struct node *node);
/*returns child position of the node, useful for deletion */
int find_which_child_am_I(struct node *node);
/*since the tree isnt modified in any way during the concurrent searches
there is in reality no need for locks which reduce the performance*/
int search(int key, struct node *node);
/*Insertion of 2-3-4 tree*/
void BSTInsert(int key);
/*used in delete to convert 2node to 3 node by stealing a sibling key*/
/*rotation*/
void steal_from_right(struct node *curr, struct node *sibling);
/*used in delete to convert 2node to 3 node by stealing a sibling key*/
/*rotation*/
void steal_from_left(struct node *curr, struct node *sibling);
/*used in delete to convert 2node to 4 node by mergin with a sibling*/
/*Merge/Fusion*/
void merge(struct node *curr, struct node *sibling);
/*used in delete if we cant convert the 2 node any other way*/
/*cant merge because parent is 2-node, cant rotate cause siblings are 2 nodes*/
/*Only happens if parent is 2-node root*/
/*Tree shrink*/
void tree_shrink();
/*returns true if key is in curr node*/
int key_in_node(struct node *curr, int key);
/*swaps key from marked node to curr leaf node*/
void swap(struct node *curr, struct node *marked, int key);
/*Deletes key from 2-3-4 tree*/
int BSTDelete(int key);


pthread_barrier_t first_barrier;
pthread_barrier_t second_barrier;
pthread_barrier_t third_barrier;
pthread_barrier_t fourth_barrier;
