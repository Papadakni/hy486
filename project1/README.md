HY486 Project 1                    |
Author:Csdp1165 Nikolaos Papadakis.|
Contact:papadakni@csd.uoc.gr       |
------------------------------------

------------------------------------
Requirements                        |
-------------------------------------
-Linux OS which supports posix threads.
-Knowledge of the programming assignment.

------------------------------------
Compiling the program               |
------------------------------------
The program is compiled via makefile,
just call make in the source folder.

------------------------------------
Running the program                 |
------------------------------------
./songmanager -t (threadcount)
e.g ./songmanager -t 100

-------------------------------------------------------------------
OVERVIEW                                                           |
-------------------------------------------------------------------
The program works as requested by the assignment which is the use
of various concurrent data structures and their use in a real 
working concurrent environment.

Everything works 100% perfectly except deleting from trees.
When using close to 100 threads the program sometimes 
doesnt perform correctly.
There must be a minor bug in the code which rarely causes 
a child of a node to not be pointing correctly.

IMPORTANT
To check performance without tree delete please comment line 340 of main.c
