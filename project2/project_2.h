#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>
#include <errno.h>
#include <time.h>
#define HASHTABLE_SIZE 509

MPI_Status status;

int world_rank; //mpi rank of each process
int world_size; //number of processes
int dummy_int;  //used to send messages where no data is required

int NUM_SERVERS;      //Number of server taken from input
int server_ids[1000]; //an array that contains the servers used by the coordinator

int int_message[5]; //used to pass intergers between messages, note the size could be
                    //smaller since only at most 2-3 values are sent at the same time
                    //usually message[0]=file_id

int right_neighbour_rank; //rank of right neighbor of server
int left_neighbour_rank;  //rank of left neighbor of server

/*leader election*/
int direct_connection_to_leader = 0;     //server has connection to leader if not zero
int leader_connections[1000];            //direct connections leader knows
int leader_id;                           //the id of the leader used by the coordinator
int have_requested_to_become_leader = 0; //leader election flag
int candidate_id = 0;
int number_of_data_in_store = 0; //counter used in some server arrays

int requested_shutdown = 0; //client remembers that it received a shutdown message
int requested_barrier = 0;  //client remembers that it received a barrier message

/*used to differentiate between different process types
since both a server and a client can receive the same
message type but have to perform differently*/
enum process_type
{
    Coordinator = 0,
    server = 1,
    client = 2,
    leader_server = 3,
    undefined = 4,
};

/*The types of messages passed between the processes of the MPI*/
enum message_types
{
    UNKNOWN,
    SERVER,
    ACK,
    START_LEADER_ELECTION,
    LEADER_ELECTION_DONE,
    INFO,
    CANDIDATE_ID,
    CONNECT,
    CLIENT,
    UPLOAD,
    UPLOAD_FAILED,
    UPLOAD_ACK,
    UPLOAD_OK,
    RETRIEVE,
    RETRIEVE_FAILED,
    RETRIEVE_ACK,
    RETRIEVE_OK,
    UPDATE,
    UPDATE_FAILED,
    UPDATE_ACK,
    UPDATE_OK,
    VERSION_CHECK,
    VERSION_OUTDATED,
    REQUEST_SHUTDOWN,
    SHUTDOWN_OK,
    LEAVE,
    LEAVE_ACK,
    LEAVE_REQUEST,
    LEAVE_CONFIRMATION,
    BARRIER,
    BARRIER_ACK,
    TRANSFER,

};

/*request type stored in the request queue of a file in the hashtable*/
enum request_type
{
    upload,
    update,
    retrieve,
};

/*queue node of the request queue*/
struct queue_node
{
    int id; //note at first I thought that id meant file id which was false
    int count;
    enum request_type type;
    int version;
    int client_requested; //but since I did not want to change the hashtable functions this is the real id, the other is really not used
    struct queue_node *next;
};

/*node of the hashtable*/
struct hashtable_node
{
    int key;
    struct queue_node *front;
    struct queue_node *back;
    struct hashtable_node *next;
};

typedef struct HashTable HashTable;

struct HashTable
{
    // Contains an array of pointers
    // to items
    struct hashtable_node **items;
    int size;
    int count;
};

/*node of list of files used by servers and clients for local storage*/
struct file_node
{
    int file_id;
    int version;
    struct file_node *next;
};

/*every process begins as undefined type 
(since it doesn't know from the start what process it is */
enum process_type my_type = undefined;

/*used to create a new priority queue node*/
struct queue_node *create_queue_node(int id);
/*used to enqueue a node in the priority queue*/
void enqueue(struct hashtable_node *node, int id, enum request_type type, int client);
/*used to dequeue the front node from the pririty queue*/
void deQueue(struct hashtable_node *node);

/*creates a new file list node*/
struct file_node *create_file_node(int id);
/*inserts a file in the file list*/
void insert_file(int id, struct file_node *head);
/*finds a file with file_id==id in the file list*/
struct file_node *find_file(int id, struct file_node *head);
/*prints the contents of the file list,used for debugging*/
void print_file_list(struct file_node *head);

/*simple hashfunction for the creation of the hashtable*/
int hashFunction(int key);
/*used to create a new hashtable item node*/
struct hashtable_node *create_item(int key);
/*used to create a new hashtable of size size*/
HashTable *create_table(int size);
/*used to insert a new file in the hashtable*/
void ht_insert(HashTable *table, int key, int client_id);
/*search and return a file (if it exists) from the hashtable*/
struct hashtable_node *hashtable_search(HashTable *table, int key);
/*prints the contents of the hashtable (used for debugging)*/
void print_hash_table(HashTable *table);

/*function used to handle a retrieve request when it appears in front of the request queue*/
void leader_request_retrieve(struct hashtable_node *curr, int server_array[]);
/*function used to handle an update request when it appears in front of the update queue*/
void leader_request_update(struct hashtable_node *curr);
/*the main loop of the coordinator, scanning the input file + all coordinator tasks and messages*/
void coordinator_main_loop(char *file_name);
