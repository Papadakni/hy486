HY486 Project 2                       |
                                      |
Author: Nikolaos Papadakis , csdp1165 |
papadakni@csd.uoc.gr                  |
                                      |
Due Date: 15-06-2020                  |
--------------------------------------



Description:       |            
--------------------
The project description is many pages long so I am going to 
be brief.Basically we have to create a file manager program
using messages with the help of the MPI library. We have 
4 types of main processes (coordinator,client,server,leader server)
and messages and actions between them,using messages to communicate
and transfer data,in order to execute file commands (upload,retrieve,update).
What I do here is that I have a main coordinator loop that handles 
the scan of the input file and every coordinator task, and a loop that
is used by any other process that performs differently according to the 
message received and the type of process. 
Every message is as described by the exercise, no new message or 
information was added.For the local file storage of the clients and 
servers I chose to use a simple list.

More info in: 
https://www.csd.uoc.gr/~hy486/current/material/projectPhases/HY-486-Project2.pdf
or if website is updated
https://www.csd.uoc.gr/~hy486/old_websites/2019-2020/material/projectPhases/HY-486-Project2.pdf


Compilation|
-----------
Simple make file 
just use make to create the 
executable.

Running the executable|
-----------------------
For my own convenience I 
created a bash_script 
./run.sh
simple uncomment whatever
line you want to run 
or simply add a new one.


