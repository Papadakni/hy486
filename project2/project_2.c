#include "project_2.h"

struct queue_node *create_queue_node(int id)
{
  struct queue_node *item = malloc(sizeof(struct queue_node));
  item->id = id;
  item->count = ((NUM_SERVERS - 1) / 2) + 1;
  item->version = 1;
  item->next = NULL;
  item->client_requested = 0;
  return item;
}

void enqueue(struct hashtable_node *node, int id, enum request_type type, int client)
{
  struct queue_node *temp = create_queue_node(id);
  temp->type = type;
  temp->client_requested = client;
  if (node->back == NULL)
  {
    node->front = node->back = temp;
    return;
  }

  // Add the new node at the end of queue and change rear
  node->back->next = temp;
  node->back = temp;
}

void deQueue(struct hashtable_node *node)
{
  // If queue is empty, return NULL.
  if (node->front == NULL)
    return;

  // Store previous front and move front one node ahead
  struct queue_node *temp = node->front;

  node->front = node->front->next;

  // If front becomes NULL, then change rear also as NULL
  if (node->front == NULL)
    node->back = NULL;

  free(temp);
}

struct file_node *create_file_node(int id)
{
  struct file_node *item = malloc(sizeof(struct file_node));
  item->file_id = id;
  item->version = 1;
  item->next = NULL;
  return item;
}

void insert_file(int id, struct file_node *head)
{
  struct file_node *current = malloc(sizeof(struct file_node));

  current = head;
  if (current == NULL)
  {
    current = create_file_node(id);
    return;
  }
  while (current->next != NULL)
  {
    current = current->next;
  }
  current->next = create_file_node(id);
}

struct file_node *find_file(int id, struct file_node *head)
{
  struct file_node *current = malloc(sizeof(struct file_node));

  current = head;
  if (current == NULL)
  {
    return NULL;
  }
  while (current != NULL)
  {
    if (current->file_id == id)
    {
      return (current);
    }
    current = current->next;
  }

  return NULL;
}

void print_file_list(struct file_node *head)
{
  struct file_node *current = malloc(sizeof(struct file_node));

  current = head;
  while (current != NULL)
  {

    printf("file :%d,version: %d \n", current->file_id, current->version);
    current = current->next;
  }
}

int hashFunction(int key)
{
  return (key % HASHTABLE_SIZE);
}

struct hashtable_node *create_item(int key)
{
  struct hashtable_node *item = malloc(sizeof(struct hashtable_node));
  item->key = key;
  item->front = NULL;
  item->back = NULL;
  item->next = NULL;

  return item;
}

HashTable *create_table(int size)
{
  int i;
  HashTable *table = (HashTable *)malloc(sizeof(HashTable));
  table->size = size;
  table->count = 0;
  table->items = (struct hashtable_node **)calloc(table->size, sizeof(struct hashtable_node));
  for (i = 0; i < table->size; i++)
    table->items[i] = NULL;

  return table;
}

void ht_insert(HashTable *table, int key, int client_id)
{
  int index = hashFunction(key);

  // Create the item
  struct hashtable_node *item = create_item(key);

  struct hashtable_node *current_item = table->items[index];
  if (current_item == NULL)
  {
    // Key does not exist.
    if (table->count == table->size)
    {
      // Hash Table Full
      printf("Insert Error: Hash Table is full\n");

      return;
    }

    // Insert directly
    table->items[index] = item;
    table->items[index]->front = create_queue_node(key);
    table->items[index]->front->type = upload;
    table->items[index]->front->client_requested = client_id;
    table->items[index]->back = table->items[index]->front;
    table->count++;
  }
  else
  {

    while (current_item != NULL)
    {
      current_item = current_item->next;
    }
    current_item = item;
    return;
  }
}

struct hashtable_node *hashtable_search(HashTable *table, int key)
{
  int index = hashFunction(key);
  struct hashtable_node *item = table->items[index];

  // Ensure that we move to a non NULL item
  while (item != NULL)
  {
    if (item->key == key)
    {
      return item;
    }
    item = item->next;
  }
  return NULL;
}

void print_hash_table(HashTable *table)
{
  int i = 0;
  printf("[ \n");
  for (i = 0; i < table->size; i++)
  {
    if (table->items[i] != NULL)
    {
      printf("FILE %d", table->items[i]->key);
      if (table->items[i]->front != NULL)
      {
        printf("with front request %d  ", table->items[i]->front->type);
      }
      printf("\n");
    }
  }
  printf("] \n");
}
void leader_request_retrieve(struct hashtable_node *curr, int server_array[])
{
  int_message[0] = curr->front->id;
  int_message[1] = curr->front->client_requested;
  int i;
  int temp_arr[NUM_SERVERS - 1];
  for (i = 0; i < number_of_data_in_store; i++)
  {
    if (server_array[i] != world_rank)
    {
      temp_arr[i] = server_array[i];
    }
  }
  int length = NUM_SERVERS - 2;
  int index;
  int where_to_send;
  int y = 0;
  int z = 0;
  int found = 0;
  for (i = 0; i < (((NUM_SERVERS - 1) / 2) + 1); i++)
  {
    index = rand() % (length);
    /*something went wrong with rand try again*/
    /* A kind of failsafe might not be needed*/
    while (index > 100 || index <= 0)
    {
      index = rand() % (length);
    }

    where_to_send = temp_arr[index];

    for (y = 0; y < NUM_SERVERS; y++)
    {
      if (server_array[y] == where_to_send)
      {
        found = 1;
      }
      if (found == 1)
      {
        for (z = 0; z < 100; z++)
        {
          if (server_array[y] == leader_connections[z] || server_array[y] == left_neighbour_rank)
          {
            //printf("CHOSE SERVER %d to send RETRIEVE request for file %d from client %d,so I ll send to %d \n", where_to_send, int_message[0], int_message[1], server_array[y]);
            int_message[2] = where_to_send;
            MPI_Send(&int_message, 3, MPI_INT, server_array[y], RETRIEVE, MPI_COMM_WORLD);
            found = 0;
            break;
          }
        }
      }
    }

    temp_arr[index] = temp_arr[length];
    //length--;
  }
  //printf("I FINISHED WITH MY RANDOM SEND!!!! \n");
}

void leader_request_update(struct hashtable_node *curr)
{

  int_message[0] = curr->key;
  int_message[1] = 1;
  int_message[2] = curr->front->version;
  MPI_Send(&int_message, 3, MPI_INT, left_neighbour_rank, VERSION_CHECK, MPI_COMM_WORLD);
}

void coordinator_main_loop(char *file_name)
{
  int i = -1;
  int j = 0;
  int z = -1;
  int server_counter = 0;
  int where_to_send;
  int is_a_server = 0;
  enum message_types message = UNKNOWN;

  my_type = Coordinator;
  FILE *testfile;
  /*check later for a better implementation
  or leave it as is if it works for all testfiles*/
  char buff[10000];
  printf("This is the file I am trying to open %s \n", file_name);

  testfile = fopen(file_name, "r");

  if (testfile == NULL)
  {
    perror("Error occurred while opening testfile text file,\n are you sure the testfile exists?.\n");
    exit(1);
  }

  /*read file */
  while (fscanf(testfile, "%s", buff) != EOF)
  {
    //printf("What is buff %s and what is i %d\n", buff,i);
    if (strcmp(buff, "SERVER") == 0)
    {

      message = SERVER;
      i = -2;
    }
    else if (strcmp(buff, "START_LEADER_ELECTION") == 0)
    {
      for (j = 0; j < NUM_SERVERS; j++)
      {
        MPI_Send(&dummy_int, 1, MPI_INT, server_ids[j], START_LEADER_ELECTION, MPI_COMM_WORLD);
      }
      printf("WAITING FOR ELECTION TO END\n");
      MPI_Recv(&leader_id, 1, MPI_INT, MPI_ANY_SOURCE, LEADER_ELECTION_DONE, MPI_COMM_WORLD, &status);

      for (j = 1; j < world_size; j++)
      {
        int_message[0] = leader_id;
        MPI_Send(&int_message, 1, MPI_INT, j, CLIENT, MPI_COMM_WORLD);
        MPI_Recv(&dummy_int, 1, MPI_INT, j, ACK, MPI_COMM_WORLD, &status);
      }
      printf("LEADER ELECTION DONE , leader_id= %d \n", leader_id);
    }
    else if (strcmp(buff, "UPLOAD") == 0)
    {
      //printf(" I found an upload message \n");
      message = UPLOAD;
      i = -1;
    }
    else if (strcmp(buff, "RETRIEVE") == 0)
    {
      //printf(" I found a retrieve message \n");
      message = RETRIEVE;
      i = -1;
    }
    else if (strcmp(buff, "UPDATE") == 0)
    {
      //printf(" I found an update message \n");
      message = UPDATE;
      i = -1;
    }
    else if (strcmp(buff, "LEAVE") == 0)
    {
      // printf(" I found a leave message \n");
      message = BARRIER;
      i = -1;
    }

    if (message == SERVER)
    {

      if (i == -1)
      {
        where_to_send = atoi(buff);
        server_ids[server_counter] = where_to_send;
        server_counter++;
      }
      if (i >= 0)
      {
        int_message[i] = atoi(buff);
      }
      if (i == 1)
      {

        MPI_Send(&int_message, 2, MPI_INT, where_to_send, SERVER, MPI_COMM_WORLD);
        MPI_Recv(&dummy_int, 1, MPI_INT, where_to_send, ACK, MPI_COMM_WORLD, &status);

        // printf("I received ACK! \n");
      }
      i++;
    }
    else if (message == UPLOAD)
    {

      if (i == 0)
      {

        where_to_send = atoi(buff);
      }
      if (i == 1)
      {
        int_message[0] = atoi(buff);
        MPI_Send(&int_message, 2, MPI_INT, where_to_send, UPLOAD, MPI_COMM_WORLD);
      }

      i++;
    }

    else if (message == RETRIEVE)
    {

      if (i == 0)
      {

        where_to_send = atoi(buff);
      }
      if (i == 1)
      {

        int_message[0] = atoi(buff);
        MPI_Send(&int_message, 2, MPI_INT, where_to_send, RETRIEVE, MPI_COMM_WORLD);
      }

      i++;
    }
    else if (message == UPDATE)
    {

      if (i == 0)
      {

        where_to_send = atoi(buff);
      }
      if (i == 1)
      {
        int_message[0] = atoi(buff);
        MPI_Send(&int_message, 2, MPI_INT, where_to_send, UPDATE, MPI_COMM_WORLD);
      }

      i++;
    }
    else if (message == BARRIER)
    {

      if (i == 0)
      {
        where_to_send = atoi(buff);
        is_a_server = 0;
        for (j = 1; j < world_size; j++)
        {
          for (z = 0; z < NUM_SERVERS; z++)
          {
            if (j == server_ids[z])
            {
              is_a_server = 1;
            }
          }
          if (is_a_server == 0)
          {
            //printf("HOW MANY TIMES I GET STUCK HERE?? \n");
            MPI_Send(&dummy_int, 1, MPI_INT, j, BARRIER, MPI_COMM_WORLD);
            MPI_Recv(&dummy_int, 1, MPI_INT, j, BARRIER_ACK, MPI_COMM_WORLD, &status);
          }
          else
          {
            is_a_server = 0;
          }
        }
        MPI_Send(&dummy_int, 1, MPI_INT, where_to_send, LEAVE, MPI_COMM_WORLD);
        MPI_Recv(&dummy_int, 1, MPI_INT, leader_id, LEAVE_ACK, MPI_COMM_WORLD, &status);
        //printf("DO I GET STUCK HERE? \n");
      }

      i++;
    }
  }
  is_a_server = 0;
  for (j = 1; j < world_size; j++)
  {
    for (z = 0; z < NUM_SERVERS; z++)
    {
      if (j == server_ids[z])
      {
        is_a_server = 1;
      }
    }
    if (is_a_server == 0)
    {

      MPI_Send(&dummy_int, 1, MPI_INT, j, REQUEST_SHUTDOWN, MPI_COMM_WORLD);
      MPI_Recv(&dummy_int, 1, MPI_INT, j, SHUTDOWN_OK, MPI_COMM_WORLD, &status);
    }
    else
    {
      is_a_server = 0;
    }
  }
  printf(" I RECEIVED SHUTDOWN FROM ALL CLIENTS! \n");
  MPI_Send(&dummy_int, 1, MPI_INT, leader_id, REQUEST_SHUTDOWN, MPI_COMM_WORLD);
  MPI_Recv(&dummy_int, 1, MPI_INT, leader_id, SHUTDOWN_OK, MPI_COMM_WORLD, &status);
  printf(" I RECEIVED SHUTDOWN FROM LEADER! \n");
}

int main(int argc, char **argv)
{
  int i;
  /*init time for rand*/
  time_t t;
  srand((unsigned)time(&t));
  // Initialize the MPI environment
  MPI_Init(&argc, &argv);

  // Get the number of processes

  MPI_Comm_size(MPI_COMM_WORLD, &world_size);

  // Get the rank of the process

  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
  NUM_SERVERS = atoi(argv[1]);

  /*if I am the process with rank 0 I am the coordinator*/
  if (world_rank == 0)
  {
    my_type = Coordinator;

    coordinator_main_loop(argv[2]);
  }
  else /*I am a potential client,server,or leader server*/
  {
    /*local var only used by servers*/
    int server_array[NUM_SERVERS];

    /*local var only used by clients*/
    int active_requests = 0;

    /*local var only used by leader*/
    struct HashTable *leader_hashtable = create_table(1000);

    /*local var used by clients and servers*/
    struct file_node *file_head = create_file_node(-1);
    while (1)
    {
      /*always recieve messages from any source*/
      MPI_Recv(&int_message, 4, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

      /*differentiate between messages using the MPI_TAG and 
      between functionality by looking what type of process I am*/

      /*received SERVER message from coordinator, I am now a server and I set 
      my left neighbor and right neighbor as the message said*/
      if (status.MPI_TAG == SERVER)
      {

        left_neighbour_rank = int_message[0];
        right_neighbour_rank = int_message[1];
        my_type = server;
        //printf("Hello I am a server %d, my left neighbor is %d and my right neighbor is %d \n", world_rank, left_neighbour_rank, right_neighbour_rank);
        MPI_Send(&dummy_int, 1, MPI_INT, 0, ACK, MPI_COMM_WORLD);
      }
      /*received leader election message from coordinator time to start it*/
      else if (status.MPI_TAG == START_LEADER_ELECTION)
      {

        if (have_requested_to_become_leader == 0)
        {
          leader_id = world_rank;
          int_message[0] = leader_id;
          MPI_Send(&int_message, 2, MPI_INT, left_neighbour_rank, CANDIDATE_ID, MPI_COMM_WORLD);
          have_requested_to_become_leader = 1;
        }
      }
      /*passing of candidate id message used in leader election*/
      else if (status.MPI_TAG == CANDIDATE_ID)
      {

        int i = 0;
        int found_in_array = 0;
        //printf("I am %d and I have received %d times,current leader is %d",world_rank,number_of_data_in_store,leader_id);
        if (have_requested_to_become_leader == 0)
        {
          leader_id = world_rank;
          int temp = int_message[0];
          int_message[0] = leader_id;
          MPI_Send(&int_message, 2, MPI_INT, left_neighbour_rank, CANDIDATE_ID, MPI_COMM_WORLD);
          have_requested_to_become_leader = 1;
          int_message[0] = temp;
        }
        if (int_message[0] > leader_id)
        {
          leader_id = int_message[0];
        }
        for (i = 0; i < number_of_data_in_store; i++)
        {
          if (server_array[i] == int_message[0])
          {
            found_in_array = 1;
          }
        }
        if (found_in_array == 0)
        {
          //printf("I AM %d AND IADDED THIS IN MY TABLE %d \n",world_rank,int_message[0]);
          server_array[number_of_data_in_store] = int_message[0];
          number_of_data_in_store++;
        }
        if (int_message[0] != world_rank)
        {
          MPI_Send(&int_message, 2, MPI_INT, left_neighbour_rank, CANDIDATE_ID, MPI_COMM_WORLD);
        }

        if (number_of_data_in_store == (NUM_SERVERS) && leader_id == world_rank)
        {

          /*printf("Please show me your buffer \n");
          for (i = 0; i < number_of_data_in_store; i++)
            {
              printf("%d \n,",server_array[i]);
            }*/

          int temp_arr[NUM_SERVERS - 3];
          my_type = leader_server;
          for (i = 0; i < number_of_data_in_store; i++)
          {
            if (server_array[i] != world_rank && server_array[i] != right_neighbour_rank && server_array[i] != left_neighbour_rank)
            {
              temp_arr[i] = server_array[i];
            }
          }
          int length = NUM_SERVERS - 4;
          int index;
          /*select random servers for direct connection*/
          for (i = 0; i < ((NUM_SERVERS - 3) / 4); i++)
          {
            index = rand() % (length);
            /*something went wrong with rand try again*/
            /* A kind of failsafe might not be needed*/
            while (index > 100 || index <= 0)
            {
              index = rand() % (length);
            }
            leader_connections[i] = temp_arr[index];
            temp_arr[index] = temp_arr[length];
            length--;

            //printf("Let me see the culprit %d \n",leader_connections[i]);
            MPI_Send(&leader_id, 1, MPI_INT, leader_connections[i], CONNECT, MPI_COMM_WORLD);
            MPI_Recv(&dummy_int, 1, MPI_INT, leader_connections[i], ACK, MPI_COMM_WORLD, &status);
          }
          for (i = ((NUM_SERVERS - 3) / 4); i < 100; i++)
          {
            leader_connections[i] = -1;
          }

          MPI_Send(&leader_id, 1, MPI_INT, 0, LEADER_ELECTION_DONE, MPI_COMM_WORLD);
        }
      }
      /*I am a server which received a connect message from leader which I save so I can use it*/
      else if (status.MPI_TAG == CONNECT)
      {
        direct_connection_to_leader = leader_id;
        MPI_Send(&dummy_int, 1, MPI_INT, direct_connection_to_leader, ACK, MPI_COMM_WORLD);
        printf("%d CONNECTED TO %d \n", world_rank, direct_connection_to_leader);
      }
      /*received a CLIENT message from coordinator from now on I am a client*/
      else if (status.MPI_TAG == CLIENT)
      {
        if (my_type == undefined)
        {
          my_type = client;
          leader_id = int_message[0];
          //printf("Hello I am %d and I am client, all hail leader %d \n", world_rank, leader_id);
        }
        MPI_Send(&dummy_int, 1, MPI_INT, 0, ACK, MPI_COMM_WORLD);
      }
      /*different actions in order to upload as detailed in the exercise according
      to process type*/
      else if (status.MPI_TAG == UPLOAD)
      {
        if (my_type == client)
        {
          //printf("Hello I am %d and I want to upload %d\n", world_rank, int_message[0]);
          insert_file(int_message[0], file_head);

          //print_file_list(file_head);

          int_message[1] = world_rank;
          MPI_Send(&int_message, 2, MPI_INT, leader_id, UPLOAD, MPI_COMM_WORLD);
          active_requests++;
          requested_barrier = 0;
        }
        else if (my_type == leader_server)
        {
          if (hashtable_search(leader_hashtable, int_message[0]) != NULL)
          {
            MPI_Send(&int_message, 2, MPI_INT, int_message[1], UPLOAD_FAILED, MPI_COMM_WORLD);
          }
          else
          {
            ht_insert(leader_hashtable, int_message[0], int_message[1]);

            int temp_arr[NUM_SERVERS - 1];
            for (i = 0; i < number_of_data_in_store; i++)
            {
              if (server_array[i] != world_rank)
              {
                temp_arr[i] = server_array[i];
              }
            }
            int length = NUM_SERVERS - 2;
            int index;
            int where_to_send;
            int y = 0;
            int z = 0;
            int found = 0;
            for (i = 0; i < (((NUM_SERVERS - 1) / 2) + 1); i++)
            {
              index = rand() % (length);
              /*something went wrong with rand try again*/
              /* A kind of failsafe might not be needed*/
              while (index > 100 || index <= 0)
              {
                index = rand() % (length);
              }

              where_to_send = temp_arr[index];
              for (y = 0; y < NUM_SERVERS; y++)
              {
                if (server_array[y] == where_to_send)
                {
                  found = 1;
                }
                if (found == 1)
                {
                  for (z = 0; z < 100; z++)
                  {
                    if (server_array[y] == leader_connections[z] || server_array[y] == left_neighbour_rank)
                    {
                      //printf("CHOSE SERVER %d to send UPLOAD request for file %d from client %d,so I ll send to %d \n", where_to_send, int_message[0], int_message[1], server_array[y]);
                      int_message[2] = where_to_send;
                      MPI_Send(&int_message, 3, MPI_INT, server_array[y], UPLOAD, MPI_COMM_WORLD);
                      found = 0;
                      break;
                    }
                  }
                }
              }

              temp_arr[index] = temp_arr[length];
              //length--;
            }
          }
        }
        else if (my_type == server)
        {
          if (int_message[2] != world_rank)
          {
            MPI_Send(&int_message, 3, MPI_INT, left_neighbour_rank, UPLOAD, MPI_COMM_WORLD);
          }
          else
          {
            insert_file(int_message[0], file_head);
            if (direct_connection_to_leader != 0)
            {
              MPI_Send(&int_message, 3, MPI_INT, direct_connection_to_leader, UPLOAD_ACK, MPI_COMM_WORLD);
            }
            else
            {
              MPI_Send(&int_message, 3, MPI_INT, left_neighbour_rank, UPLOAD_ACK, MPI_COMM_WORLD);
            }
          }
        }
      }
      /*received upload failed from leader server because file was already
      uploaded (in the leader hashtable)*/
      else if (status.MPI_TAG == UPLOAD_FAILED)
      {
        printf("CLIENT <%d> FAILED TO UPLOAD <%d> \n", world_rank, int_message[0]);
        active_requests--;
        if ((active_requests == 0) && (requested_shutdown == 1))
        {
          MPI_Send(&dummy_int, 1, MPI_INT, 0, SHUTDOWN_OK, MPI_COMM_WORLD);
          break;
        }
        if ((active_requests == 0) && (requested_barrier == 1))
        {
          MPI_Send(&dummy_int, 1, MPI_INT, 0, BARRIER_ACK, MPI_COMM_WORLD);
        }
      }
      /*used in uploading by the servers */
      else if (status.MPI_TAG == UPLOAD_ACK)
      {
        if (my_type == leader_server)
        {
          struct hashtable_node *curr = hashtable_search(leader_hashtable, int_message[0]);
          curr->front->count--;

          if (curr->front->count == 0)
          {
            MPI_Send(&int_message, 3, MPI_INT, curr->front->client_requested, UPLOAD_OK, MPI_COMM_WORLD);
            //pop and commit new request?
            deQueue(curr);
            if (curr->front != NULL)
            {
              if (curr->front->type == retrieve)
              {
                leader_request_retrieve(curr, server_array);
              }
              else if (curr->front->type == update)
              {
                leader_request_update(curr);
              }
            }
          }
        }
        else
        {
          MPI_Send(&int_message, 3, MPI_INT, left_neighbour_rank, UPLOAD_ACK, MPI_COMM_WORLD);
        }
      }
      /*received upload ok from leader server since all selected servers
      handled the upload request*/
      else if (status.MPI_TAG == UPLOAD_OK)
      {
        printf("CLIENT <%d> UPLOADED <%d> \n", int_message[1], int_message[0]);
        active_requests--;
        if ((active_requests == 0) && (requested_shutdown == 1))
        {
          MPI_Send(&dummy_int, 1, MPI_INT, 0, SHUTDOWN_OK, MPI_COMM_WORLD);
          break;
        }
        if ((active_requests == 0) && (requested_barrier == 1))
        {
          MPI_Send(&dummy_int, 1, MPI_INT, 0, BARRIER_ACK, MPI_COMM_WORLD);
        }
      }
      /*different actions in order to retrieve as detailed in the exercise according
      to process type*/
      else if (status.MPI_TAG == RETRIEVE)
      {
        if (my_type == client)
        {
          //printf("Hello I am %d and I want to retrieve %d\n", world_rank, int_message[0]);
          int_message[1] = world_rank;
          MPI_Send(&int_message, 2, MPI_INT, leader_id, RETRIEVE, MPI_COMM_WORLD);
          active_requests++;
          requested_barrier = 0;
        }
        else if (my_type == leader_server)
        {
          // print_hash_table(leader_hashtable);
          //printf("I RECIEVED RETRIEVE REQUEST for file %d!\n",int_message[0]);
          if (hashtable_search(leader_hashtable, int_message[0]) == NULL)
          {
            MPI_Send(&int_message, 2, MPI_INT, int_message[1], RETRIEVE_FAILED, MPI_COMM_WORLD);
          }
          else
          {
            struct hashtable_node *temp = hashtable_search(leader_hashtable, int_message[0]);
            if (temp->front != NULL)
            {
              // printf("EDW LOGIKA DEN MPAINW?\n");
              enqueue(temp, int_message[0], retrieve, int_message[1]);
            }
            else
            {
              //  printf("MHPWS EDW??? \n");
              enqueue(temp, int_message[0], retrieve, int_message[1]);

              int temp_arr[NUM_SERVERS - 1];
              for (i = 0; i < number_of_data_in_store; i++)
              {
                if (server_array[i] != world_rank)
                {
                  temp_arr[i] = server_array[i];
                }
              }
              int length = NUM_SERVERS - 2;
              int index;
              int where_to_send;
              int y = 0;
              int z = 0;
              int found = 0;
              for (i = 0; i < (((NUM_SERVERS - 1) / 2) + 1); i++)
              {
                index = rand() % (length);
                /*something went wrong with rand try again*/
                /* A kind of failsafe might not be needed*/
                while (index > 100 || index <= 0)
                {
                  index = rand() % (length);
                }

                where_to_send = temp_arr[index];
                //printf("WHICH SERVER I CHOSE %d ??? \n ",where_to_send);
                for (y = 0; y < NUM_SERVERS; y++)
                {
                  if (server_array[y] == where_to_send)
                  {

                    found = 1;
                  }
                  if (found == 1)
                  {
                    for (z = 0; z < 100; z++)
                    {
                      if (server_array[y] == leader_connections[z] || server_array[y] == left_neighbour_rank)
                      {

                        //printf("CHOSE SERVER %d to send RETRIEVE request for file %d from client %d,so I ll send to %d \n", where_to_send, int_message[0], int_message[1], server_array[y]);
                        int_message[2] = where_to_send;
                        MPI_Send(&int_message, 3, MPI_INT, server_array[y], RETRIEVE, MPI_COMM_WORLD);
                        found = 0;
                        break;
                      }
                    }
                  }
                }

                temp_arr[index] = temp_arr[length];
                //length--;
              }
            }
          }
        }
        else if (my_type == server)
        {
          if (int_message[2] != world_rank)
          {
            MPI_Send(&int_message, 3, MPI_INT, left_neighbour_rank, RETRIEVE, MPI_COMM_WORLD);
          }
          else
          {
            struct file_node *temp = malloc(sizeof(struct file_node));
            temp = find_file(int_message[0], file_head);
            if (temp != NULL)
            {
              int_message[3] = temp->version;
            }
            else
            {
              int_message[3] = 0;
            }

            if (direct_connection_to_leader != 0)
            {
              MPI_Send(&int_message, 4, MPI_INT, direct_connection_to_leader, RETRIEVE_ACK, MPI_COMM_WORLD);
            }
            else
            {
              MPI_Send(&int_message, 4, MPI_INT, left_neighbour_rank, RETRIEVE_ACK, MPI_COMM_WORLD);
            }
          }
        }
      }
      /*leader did not find the file and thus retrieve failed message was sent*/
      else if (status.MPI_TAG == RETRIEVE_FAILED)
      {
        printf("CLIENT <%d> FAILED TO RETRIEVE <%d> \n", world_rank, int_message[0]);
        active_requests--;
        if ((active_requests == 0) && (requested_shutdown == 1))
        {
          MPI_Send(&dummy_int, 1, MPI_INT, 0, SHUTDOWN_OK, MPI_COMM_WORLD);
          break;
        }
        if ((active_requests == 0) && (requested_barrier == 1))
        {
          MPI_Send(&dummy_int, 1, MPI_INT, 0, BARRIER_ACK, MPI_COMM_WORLD);
        }
      }
      else if (status.MPI_TAG == RETRIEVE_ACK)
      {

        if (my_type == leader_server)
        {
          struct hashtable_node *curr = hashtable_search(leader_hashtable, int_message[0]);
          curr->front->count--;

          if (int_message[3] > curr->front->version)
          {
            curr->front->version = int_message[3];
          }
          if (curr->front->count == 0)
          {
            int_message[3] = curr->front->version;
            MPI_Send(&int_message, 4, MPI_INT, curr->front->client_requested, RETRIEVE_OK, MPI_COMM_WORLD);
            //pop and commit new request?
            deQueue(curr);
            if (curr->front != NULL)
            {
              if (curr->front->type == retrieve)
              {
                leader_request_retrieve(curr, server_array);
              }
              else if (curr->front->type == update)
              {
                leader_request_update(curr);
              }
            }
          }
        }
        else
        {
          //printf("HELLO I AM SERVER %d and I got a retrieve ack I am sending it to %d \n",world_rank,left_neighbour_rank);
          MPI_Send(&int_message, 4, MPI_INT, left_neighbour_rank, RETRIEVE_ACK, MPI_COMM_WORLD);
        }
      }
      /*retrieve was successfull*/
      else if (status.MPI_TAG == RETRIEVE_OK)
      {
        printf("CLIENT <%d> RETRIEVED VERSION <%d> OF <%d> \n", world_rank, int_message[3], int_message[0]);
        struct file_node *temp = malloc(sizeof(struct file_node));
        temp = find_file(int_message[0], file_head);
        if (temp != NULL)
        {
          temp->version = int_message[3];
        }
        else
        {
          insert_file(int_message[0], file_head);
        }
        temp = find_file(int_message[0], file_head);
        temp->version = int_message[3];

        //printf("SHOW ME WHAT %d HAS AFTER RETRIEVING \n",world_rank);
        // print_file_list(file_head);
        // printf("\n");
        active_requests--;
        if ((active_requests == 0) && (requested_shutdown == 1))
        {
          MPI_Send(&dummy_int, 1, MPI_INT, 0, SHUTDOWN_OK, MPI_COMM_WORLD);
          break;
        }
        if ((active_requests == 0) && (requested_barrier == 1))
        {
          MPI_Send(&dummy_int, 1, MPI_INT, 0, BARRIER_ACK, MPI_COMM_WORLD);
        }
      }
      /*different actions in order to update as detailed in the exercise according
      to process type*/
      else if (status.MPI_TAG == UPDATE)
      {
        if (my_type == client)
        {
          //printf("Hello I am %d and I want to update %d\n", world_rank, int_message[0]);
          int_message[1] = world_rank;
          struct file_node *temp = malloc(sizeof(struct file_node));
          temp = find_file(int_message[0], file_head);
          if (temp != NULL)
          {
            int_message[3] = temp->version;
          }
          else
          {
            int_message[3] = 0;
          }
          //printf("SHOW ME WHAT %d HAS BEFORE REQUESTING UPDATE \n",world_rank);
          //print_file_list(file_head);
          //printf("\n");
          MPI_Send(&int_message, 4, MPI_INT, leader_id, UPDATE, MPI_COMM_WORLD);
          active_requests++;
          requested_barrier = 0;
        }
        else if (my_type == leader_server)
        {
          //printf("I RECIEVED UPDATE REQUEST for file %d!\n",int_message[0]);

          if (hashtable_search(leader_hashtable, int_message[0]) == NULL)
          {
            MPI_Send(&int_message, 2, MPI_INT, int_message[1], UPDATE_FAILED, MPI_COMM_WORLD);
          }
          else
          {
            struct hashtable_node *temp = hashtable_search(leader_hashtable, int_message[0]);
            if (temp->front != NULL)
            {

              enqueue(temp, int_message[0], update, int_message[1]);
              temp->front->version = int_message[3];
            }
            else
            {

              enqueue(temp, int_message[0], update, int_message[1]);
              int_message[1] = 1;
              int_message[2] = int_message[3];
              MPI_Send(&int_message, 3, MPI_INT, left_neighbour_rank, VERSION_CHECK, MPI_COMM_WORLD);
            }
          }
        }
        else if (my_type == server)
        {
          //printf("I AM server %d  AND I RECIEVED UPDATE REQUEST \n",world_rank);
          if (int_message[3] != world_rank)
          {
            MPI_Send(&int_message, 4, MPI_INT, left_neighbour_rank, UPDATE, MPI_COMM_WORLD);
          }
          else
          {
            struct file_node *temp = malloc(sizeof(struct file_node));
            temp = find_file(int_message[0], file_head);
            if (temp != NULL)
            {
              if (temp->version <= int_message[2])
              {
                temp->version = int_message[2] + 1;
              }
            }
            else
            {
              insert_file(int_message[0], file_head);
              temp = find_file(int_message[0], file_head);
              temp->version = int_message[2] + 1;
            }

            if (direct_connection_to_leader != 0)
            {
              MPI_Send(&int_message, 4, MPI_INT, direct_connection_to_leader, UPDATE_ACK, MPI_COMM_WORLD);
            }
            else
            {
              MPI_Send(&int_message, 4, MPI_INT, left_neighbour_rank, UPDATE_ACK, MPI_COMM_WORLD);
            }
          }
        }
      }
      else if (status.MPI_TAG == UPDATE_ACK)
      {

        if (my_type == leader_server)
        {

          struct hashtable_node *curr = hashtable_search(leader_hashtable, int_message[0]);
          curr->front->count--;

          if (curr->front->count == 0)
          {
            int_message[3] = curr->front->version;
            MPI_Send(&int_message, 4, MPI_INT, curr->front->client_requested, UPDATE_OK, MPI_COMM_WORLD);
            //pop and commit new request?
            deQueue(curr);
            if (curr->front != NULL)
            {
              if (curr->front->type == retrieve)
              {
                leader_request_retrieve(curr, server_array);
              }
              else if (curr->front->type == update)
              {
                leader_request_update(curr);
              }
            }
          }
        }
        else
        {
          //printf("WHERE DO I SEND UPDATE ACKK ?? %d \n",left_neighbour_rank);
          MPI_Send(&int_message, 4, MPI_INT, left_neighbour_rank, UPDATE_ACK, MPI_COMM_WORLD);
        }
      }
      else if (status.MPI_TAG == VERSION_CHECK)
      {
        if (my_type == server)
        {
          //printf("I AM %d and I version check %d \n",world_rank,int_message[0]);
          if (int_message[1] == 1)
          {

            struct file_node *temp = malloc(sizeof(struct file_node));
            temp = find_file(int_message[0], file_head);
            if (temp == NULL)
            {
              MPI_Send(&int_message, 3, MPI_INT, left_neighbour_rank, VERSION_CHECK, MPI_COMM_WORLD);
            }
            else if (temp->version <= int_message[2])
            {

              MPI_Send(&int_message, 3, MPI_INT, left_neighbour_rank, VERSION_CHECK, MPI_COMM_WORLD);
            }
            else
            {

              int_message[1] = 0;
              MPI_Send(&int_message, 3, MPI_INT, left_neighbour_rank, VERSION_CHECK, MPI_COMM_WORLD);
            }
          }
          else
          {
            MPI_Send(&int_message, 3, MPI_INT, left_neighbour_rank, VERSION_CHECK, MPI_COMM_WORLD);
          }
        }
        else if (my_type == leader_server)
        {

          if (int_message[1] == 0)
          {
            // printf("ILLEGAL VERSION!!!! \n");
            struct hashtable_node *temp = hashtable_search(leader_hashtable, int_message[0]);
            MPI_Send(&int_message, 1, MPI_INT, temp->front->client_requested, VERSION_OUTDATED, MPI_COMM_WORLD);
            deQueue(temp);
            if (temp->front != NULL)
            {
              if (temp->front->type == retrieve)
              {
                leader_request_retrieve(temp, server_array);
              }
              else if (temp->front->type == update)
              {
                leader_request_update(temp);
              }
            }
          }
          else
          {
            // printf("OK DONE WITH VERSION CHECK!!!! \n");
            int temp_arr[NUM_SERVERS - 1];
            for (i = 0; i < number_of_data_in_store; i++)
            {
              if (server_array[i] != world_rank)
              {
                temp_arr[i] = server_array[i];
              }
            }
            int length = NUM_SERVERS - 2;
            int index;
            int where_to_send;
            int y = 0;
            int z = 0;
            int found = 0;
            for (i = 0; i < (((NUM_SERVERS - 1) / 2) + 1); i++)
            {
              index = rand() % (length);
              /*something went wrong with rand try again*/
              /* A kind of failsafe might not be needed*/
              while (index > 100 || index <= 0)
              {
                index = rand() % (length);
              }

              where_to_send = temp_arr[index];
              for (y = 0; y < NUM_SERVERS; y++)
              {
                if (server_array[y] == where_to_send)
                {
                  //printf("FOUND THE SERVER FOR THE UPDATE \n");
                  found = 1;
                }
                if (found == 1)
                {
                  for (z = 0; z < 100; z++)
                  {
                    if (server_array[y] == leader_connections[z] || server_array[y] == left_neighbour_rank)
                    {
                      //printf("CHOSE SERVER %d to send UPDATE request for file %d from client %d,so I ll send to %d \n", where_to_send, int_message[0], int_message[1], server_array[y]);
                      int_message[3] = where_to_send;
                      MPI_Send(&int_message, 4, MPI_INT, server_array[y], UPDATE, MPI_COMM_WORLD);
                      found = 0;
                      break;
                    }
                  }
                }
              }

              temp_arr[index] = temp_arr[length];
              //length--;
            }
          }
        }
      }
      else if (status.MPI_TAG == UPDATE_FAILED)
      {
        printf("CLIENT <%d> UPDATE FAILED <%d> \n", world_rank, int_message[0]);
        active_requests--;
        if ((active_requests == 0) && (requested_shutdown == 1))
        {
          MPI_Send(&dummy_int, 1, MPI_INT, 0, SHUTDOWN_OK, MPI_COMM_WORLD);
          break;
        }
        if ((active_requests == 0) && (requested_barrier == 1))
        {
          MPI_Send(&dummy_int, 1, MPI_INT, 0, BARRIER_ACK, MPI_COMM_WORLD);
        }
      }
      else if (status.MPI_TAG == UPDATE_OK)
      {
        printf("CLIENT <%d> UPDATED <%d> \n", world_rank, int_message[0]);
        struct file_node *temp = malloc(sizeof(struct file_node));
        temp = find_file(int_message[0], file_head);
        if (temp != NULL)
        {
          temp->version++;
        }
        active_requests--;
        if ((active_requests == 0) && (requested_shutdown == 1))
        {
          MPI_Send(&dummy_int, 1, MPI_INT, 0, SHUTDOWN_OK, MPI_COMM_WORLD);
          break;
        }
        if ((active_requests == 0) && (requested_barrier == 1))
        {
          MPI_Send(&dummy_int, 1, MPI_INT, 0, BARRIER_ACK, MPI_COMM_WORLD);
        }
      }
      /*found a file in a server with a version higher than what the client had*/
      else if (status.MPI_TAG == VERSION_OUTDATED)
      {
        ("CLIENT <%d> CANNOT UPDATE  <%d> WITHOUT MOST RECENT VERSION\n", world_rank, int_message[0]);
        active_requests--;
        if ((active_requests == 0) && (requested_shutdown == 1))
        {
          MPI_Send(&dummy_int, 1, MPI_INT, 0, SHUTDOWN_OK, MPI_COMM_WORLD);
          break;
        }
        if ((active_requests == 0) && (requested_barrier == 1))
        {
          MPI_Send(&dummy_int, 1, MPI_INT, 0, BARRIER_ACK, MPI_COMM_WORLD);
        }
      }
      /*coordinator sent a shutdown request*/
      else if (status.MPI_TAG == REQUEST_SHUTDOWN)
      {
        if (my_type == client)
        {

          requested_shutdown = 1;
          if ((active_requests == 0) && (requested_shutdown == 1))
          {
            MPI_Send(&dummy_int, 1, MPI_INT, 0, SHUTDOWN_OK, MPI_COMM_WORLD);
            break;
          }
        }
        else if (my_type == leader_server)
        {
          if (requested_shutdown == 0)
          {
            MPI_Send(&dummy_int, 1, MPI_INT, left_neighbour_rank, REQUEST_SHUTDOWN, MPI_COMM_WORLD);
          }
          else
          {
            MPI_Send(&dummy_int, 1, MPI_INT, 0, SHUTDOWN_OK, MPI_COMM_WORLD);
            break;
          }
          requested_shutdown = 1;
        }
        else if (my_type == server)
        {
          MPI_Send(&dummy_int, 1, MPI_INT, left_neighbour_rank, REQUEST_SHUTDOWN, MPI_COMM_WORLD);
          break;
        }
      }
      /*coordinator sent a barrier request and is awaiting a barrier ack*/
      else if (status.MPI_TAG == BARRIER)
      {
        if (my_type == client)
        {

          requested_barrier = 1;
          if ((active_requests == 0) && (requested_barrier == 1))
          {
            MPI_Send(&dummy_int, 1, MPI_INT, 0, BARRIER_ACK, MPI_COMM_WORLD);
          }
        }
      }
      /*server received a leave message */
      else if (status.MPI_TAG == LEAVE)
      {
        if (my_type == server)
        {
          int_message[0] = world_rank;
          if (direct_connection_to_leader != 0)
          {
            MPI_Send(&int_message, 4, MPI_INT, direct_connection_to_leader, LEAVE_REQUEST, MPI_COMM_WORLD);
          }
          else
          {
            MPI_Send(&int_message, 4, MPI_INT, left_neighbour_rank, LEAVE_REQUEST, MPI_COMM_WORLD);
          }
        }
      }
      else if (status.MPI_TAG == LEAVE_REQUEST)
      {
        if (my_type == server)
        {
          if (direct_connection_to_leader != 0)
          {
            MPI_Send(&int_message, 4, MPI_INT, direct_connection_to_leader, LEAVE_REQUEST, MPI_COMM_WORLD);
          }
          else
          {
            MPI_Send(&int_message, 4, MPI_INT, left_neighbour_rank, LEAVE_REQUEST, MPI_COMM_WORLD);
          }
        }
        if (my_type == leader_server)
        {
          int y;
          int found = 0;
          //printf("I AM %d AND I WANT TO REMOVE %d \n",world_rank, int_message[0]);
          // printf("HERE IS MY SERVER ARRAY! \n");
          for (y = 0; y < NUM_SERVERS; y++)
          {
            if (server_array[y] == int_message[0])
            {
              found = 1;
              NUM_SERVERS = NUM_SERVERS - 1;
              number_of_data_in_store--;
            }
            if (found == 1)
            {
              server_array[y] = server_array[y + 1];
            }
            //printf("SERVER:%d,\n", server_array[y]);
          }
          MPI_Send(&int_message, 4, MPI_INT, left_neighbour_rank, LEAVE_CONFIRMATION, MPI_COMM_WORLD);
        }
      }
      else if (status.MPI_TAG == LEAVE_CONFIRMATION)
      {
        if (my_type == server)
        {
          //printf("I AM %d AND I WANT TO REMOVE %d \n",world_rank, int_message[0]);
          if (world_rank == int_message[0])
          {
            struct file_node *current = malloc(sizeof(struct file_node));

            current = file_head;
            while (current != NULL)
            {
              int_message[0] = current->file_id;
              int_message[1] = current->version;
              MPI_Send(&int_message, 4, MPI_INT, left_neighbour_rank, TRANSFER, MPI_COMM_WORLD);
              current = current->next;
            }
            int_message[0] = world_rank;
            MPI_Send(&int_message, 4, MPI_INT, left_neighbour_rank, LEAVE_CONFIRMATION, MPI_COMM_WORLD);
            break;
          }
          else
          {
            int y;
            int found = 0;
            int left_neigh = 0;
            for (y = 0; y < NUM_SERVERS; y++)
            {
              if (server_array[y] == int_message[0])
              {
                found = 1;
                NUM_SERVERS = NUM_SERVERS - 1;
                //printf("I AM %d AND THE NUM SERVERS IS %d \n",world_rank, NUM_SERVERS);
                if (server_array[y] == left_neighbour_rank)
                {
                  left_neigh = 1;
                  left_neighbour_rank = server_array[y - 1];
                }
                if (server_array[y] == right_neighbour_rank)
                {
                  right_neighbour_rank = server_array[y + 1];
                }
              }
              if (found == 1)
              {
                server_array[y] = server_array[y + 1];
              }
              //printf("SERVER:%d,\n", server_array[y]);
            }
            //printf("I AM %d my left is %d and my right is %d \n",world_rank,left_neighbour_rank,right_neighbour_rank);
            if (left_neigh == 1)
            {
              MPI_Send(&int_message, 4, MPI_INT, int_message[0], LEAVE_CONFIRMATION, MPI_COMM_WORLD);
            }
            else
            {
              MPI_Send(&int_message, 4, MPI_INT, left_neighbour_rank, LEAVE_CONFIRMATION, MPI_COMM_WORLD);
            }
          }
        }
        else if (my_type == leader_server)
        {
          int z;

          for (z = 0; z < 100; z++)
          {
            if (int_message[0] == leader_connections[z])
            {
              leader_connections[z] = -1;
            }
          }
          if (int_message[0] == left_neighbour_rank)
          {
            left_neighbour_rank = server_array[NUM_SERVERS - 2];
          }
          printf("<%d> HAS LEFT THE SYSTEM \n", int_message[0]);

          //print_hash_table(leader_hashtable);

          MPI_Send(&dummy_int, 1, MPI_INT, 0, LEAVE_ACK, MPI_COMM_WORLD);
        }
      }
      /*transferal of the entirety of local data contained in the leaving server
      part by part*/
      else if (status.MPI_TAG == TRANSFER)
      {
        if (my_type == server)
        {
          struct file_node *temp = malloc(sizeof(struct file_node));
          temp = find_file(int_message[0], file_head);
          if (temp != NULL)
          {
            if (temp->version < int_message[1])
            {
              temp->version = int_message[1];
            }
          }
          else
          {
            insert_file(int_message[0], file_head);
          }
          temp = find_file(int_message[0], file_head);
          if (temp->version < int_message[1])
          {
            temp->version = int_message[1];
          }
        }
      }
    }
  }
  /*all is good all have finished time to mpi_finalize*/
  MPI_Finalize();
}